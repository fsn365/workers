import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { MinerInfo } from '../models/miner.model';
import BlockDoc from './block.interface';

@Injectable()
export class MongoService {
  private logger = new Logger(`BlockWorker:MongoService`);

  constructor(@InjectModel('Blocks') private readonly model: Model<BlockDoc>) {}

  async aggregateMiners(
    prevTrackAt: number,
  ): Promise<{ syncBk: number; miners: MinerInfo[] }> {
    this.logger.log(`Aggregate miners. Prev track at: ${prevTrackAt}`);
    let syncBk = prevTrackAt;
    const miners = await this.model
      .aggregate([
        { $match: { number: { $gt: prevTrackAt } } },
        {
          $group: {
            _id: '$miner',
            latest: { $max: '$number' },
            timestamp: { $min: '$timestamp' },
          },
        },
      ])
      .then(stats => {
        this.logger.log(`Found ${stats.length} miners this time.`);
        return stats.map(({ _id, latest, timestamp }) => {
          syncBk = Math.max(latest, syncBk);
          return { address: _id, timestamp };
        });
      })
      .catch(e => {
        this.logger.log(`Aggregate miners failed.`);
        this.logger.error(JSON.stringify(e));
        return [];
      });
    return { syncBk, miners };
  }
}
