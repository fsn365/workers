import { Injectable, Inject, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { from } from 'rxjs';
import { AddressAssetMsg } from './msg.interface';
import { SERVICE_NAME } from './address.client.provider';
import { MinerInfo } from '../models/miner.model';

@Injectable()
export class AddressService {
  private logger = new Logger('Block Worker:AddressService');

  constructor(@Inject(SERVICE_NAME) private readonly client: ClientProxy) {}

  updateMinersFsnBalance(miners: MinerInfo[]): void {
    from(miners).subscribe((miner: MinerInfo) => {
      const { address, timestamp } = miner;
      const msg: AddressAssetMsg = {
        s: address,
        miner: true,
        timestamp,
      };
      this.client.emit('address:balance', msg);
      this.logger.log(`address:balance`);
      this.logger.log(JSON.stringify(msg));
    });
  }
}
