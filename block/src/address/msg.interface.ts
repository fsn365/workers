export interface AddressAssetMsg {
  s: string;
  timestamp: number;
  miner: true;
}
