import { Module } from '@nestjs/common';
import { AddressService } from './address.service';
import { ADDRESS_CLIENT_PROVIDER } from './address.client.provider';

@Module({
  providers: [ADDRESS_CLIENT_PROVIDER, AddressService],
  exports: [AddressService],
})
export class AddressModule {}
