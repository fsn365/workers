import { ConfigService } from '@nestjs/config';
import { Transport, ClientProxyFactory } from '@nestjs/microservices';

export const SERVICE_NAME = 'ADDRESS_SERVICE';

export const ADDRESS_CLIENT_PROVIDER = {
  provide: SERVICE_NAME,
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => {
    const options = configService.get('address');
    const option = {
      name: SERVICE_NAME,
      transport: Transport.RMQ,
      options,
    };
    return ClientProxyFactory.create(option);
  },
};
