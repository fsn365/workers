import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { config } from './config';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const CONFIG = config();
  const logger = new Logger(CONFIG.app.name);
  const app = await NestFactory.createMicroservice(AppModule, CONFIG.app);

  const msg = `\n\n
=========================================================
        ${CONFIG.app.name} service started...
=========================================================
    `;

  await app.listen(() => logger.log(msg));
}
bootstrap();
