import { Transport } from '@nestjs/microservices';

export const config = () => ({
  address: {
    urls: [process.env.address_rabbitmq_url],
    queue: process.env.address_rabbitmq_queue,
    prefetchCount: 1,
    noAck: false,
    queueOptions: {
      durable: true,
    },
  },
  mongodb: {
    uri: process.env['mongo_uri'],
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  redis: {
    name: 'block',
    host: process.env.redis_host || 'localhost',
    port: process.env.redis_port || 6379,
  },
  app: {
    name: 'Block worker',
    transport: Transport.TCP,
    options: {
      host: process.env.app_host || '127.0.0.1',
      port: process.env.app_port || 8888,
    },
  },
});
