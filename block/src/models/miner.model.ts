export interface MinerInfo {
  address: string;
  timestamp: number;
}
