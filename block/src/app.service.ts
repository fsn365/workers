import { Injectable, Logger } from '@nestjs/common';
import { RedisService } from 'nestjs-redis';
import { Interval } from '@nestjs/schedule';
import { MongoService } from './mongo/mongo.service';
import { AddressService } from './address/address.service';

@Injectable()
export class AppService {
  private logger = new Logger(`Block Worker:AppService`);
  private redis: any;

  constructor(
    private readonly mongo: MongoService,
    private readonly redisService: RedisService,
    private readonly addressService: AddressService,
  ) {
    this.redis = this.redisService.getClient('block');
  }

  @Interval(6000)
  private async handleInterval() {
    this.trackMiners();
  }

  private async trackMiners() {
    const prevSyncAt = await this.getMinersSyncHeight();
    const { miners, syncBk } = await this.mongo.aggregateMiners(prevSyncAt);
    if (miners.length) {
      this.addressService.updateMinersFsnBalance(miners);
      let t1 = setTimeout(() => {
        clearTimeout(t1);
        t1 = null;
        this.setMinersSyncHeight(syncBk);
      }, 10);
    }
  }

  private getMinersSyncHeight() {
    const redisKey = this.getTrackAtkey();
    return this.redis
      .get(redisKey)
      .then((val: string) => {
        if (val) return +val;
        return -1;
      })
      .catch((e: any) => {
        this.logger.log(`Get bk:track_at error:`);
        this.logger.error(e);
        return -1;
      });
  }

  private setMinersSyncHeight(height: number) {
    const redisKey = this.getTrackAtkey();
    this.redis.set(redisKey, height);
  }

  private getTrackAtkey() {
    return 'bk:track_at';
  }
}
