import { Injectable } from '@nestjs/common';
import { PgService } from './pg';
import {
  TxTlBalanceMsg,
  TxBalanceMsg,
  ITokenBalance,
  ITokenTlBalance,
  PgTokenPrevBalance,
  MsgBalancePair,
} from './models';
import { FSN_TOKEN, FSN_TOKEN1 } from './constant';
import { CustomLogger } from './common';
import { TokenService } from './token';

@Injectable()
export class AppService extends CustomLogger {
  constructor(private pg: PgService, private token: TokenService) {
    super('AppService');
  }

  async processTxBalanceMsg(msg: TxBalanceMsg): Promise<boolean> {
    this.logInfo({
      method: `processTxBalance`,
      pattern: 'balance',
      data: msg,
    });

    const provider = this.pg.getTrxProvider();
    const trx = await provider();

    const { s, r } = msg;
    const assets = this.delInvalidAsset(msg.assets);
    const promises = [];

    const assetsForSender = Array.from(new Set([...assets, FSN_TOKEN]));
    const pair = { address: s, assets: assetsForSender };
    const processTxSender = this.processBalancePair(pair, provider);
    promises.push(processTxSender);

    if (r) {
      const pair = { address: r, assets };
      const processTxReceiver = this.processBalancePair(pair, provider);
      promises.push(processTxReceiver);
    }

    return Promise.all(promises)
      .then(result => !new Set(result).has(false))
      .then(success => {
        if (success) trx.commit();
        else trx.rollback();
        return success;
      })
      .catch(e => {
        trx.rollback();
        this.logError({ method: 'processTxBalanceMsg', data: msg, e });
        return false;
      });
  }

  async processTlTxBalanceMsg(msg: TxTlBalanceMsg): Promise<boolean> {
    this.logInfo({
      method: 'processTlTxBalanceMsg',
      pattern: 'tl_balance',
      data: msg,
    });

    const provider = this.pg.getTrxProvider();
    const trx = await provider();

    const { s, r } = msg;
    const assets = this.delInvalidAsset(msg.assets);

    const promises: any[] = [];

    const fsnPair = { address: s, assets: [FSN_TOKEN] };
    const updateSendersFsn = this.processBalancePair(fsnPair, provider);
    promises.push(updateSendersFsn);

    const pair = { address: s, assets };
    const updateSender = this.processTlBalancePair(pair, provider);
    promises.push(updateSender);

    if (r) {
      const pair = { address: r, assets };
      const updateRecevier = this.processTlBalancePair(pair, provider);
      promises.push(updateRecevier);
    }

    return Promise.all(promises)
      .then(result => !new Set(result).has(false))
      .then(success => {
        if (success) trx.commit();
        else trx.rollback();
        return success;
      })
      .catch(e => {
        this.logError({ method: 'processTlTxBalance', data: msg, e });
        trx.rollback();
        return false;
      });
  }

  logInfo(info: { method: string; data?: any; pattern: string }) {
    const { method, data, pattern } = info;
    this.logInfoMsg(pattern);
    super.logInfo({ method, data });
  }

  private async processBalancePair(
    pair: MsgBalancePair,
    provider: any,
  ): Promise<boolean> {
    const { address, assets = [] } = pair;
    if (assets.length === 0) return true;

    const getBalances = assets.map(asset =>
      this.token.getTokenBalance({ asset, address }),
    );
    const balances = await Promise.all(getBalances);
    const trackBalances = balances.map(balance =>
      this.trackBalance(balance, provider),
    );

    return Promise.all(trackBalances).then(
      result => !new Set(result).has(false),
    );
  }

  private async processTlBalancePair(
    pair: MsgBalancePair,
    provider: any,
  ): Promise<boolean> {
    const { address, assets = [] } = pair;
    if (assets.length === 0) return true;

    const getTlBalances = assets.map(asset =>
      this.token.getTokenTlBalance({ address, asset }),
    );
    const balances = await Promise.all(getTlBalances);

    const trackTlBalances = balances.map(balance =>
      this.trackTlBalance(balance, provider),
    );

    return Promise.all(trackTlBalances).then(
      result => !new Set(result).has(false),
    );
  }

  private async trackBalance(balance: any, provider: any): Promise<boolean> {
    if (!balance) return true;

    const isExist = await this.pg.checkExistence(balance, provider);
    const qtyOwn = this.calcQtyOwn(balance, isExist);
    const record = { ...balance };

    let check = balance.qty;
    const isFusionToken = balance.asset.length !== 42;
    if (isFusionToken) {
      record.qty_own = qtyOwn;
      check = qtyOwn;
    }

    if (check) {
      return isExist
        ? this.pg.updateBalanceRecord(record, provider)
        : this.createBalanceRecord(record, provider);
    }

    return isExist ? this.delBalanceRecord(balance, provider) : true;
  }

  private async trackTlBalance(
    balance: ITokenTlBalance,
    provider: any,
  ): Promise<boolean> {
    if (!balance) return true;

    const { data = [], address, asset, qty_in } = balance;
    const isExist = await this.pg.checkExistence(balance, provider);
    const isEmptyRecord = data.length === 0;

    const promises: any[] = [];
    if (qty_in !== undefined) {
      const balance = { asset, address, qty_in };
      const trackBalance = this.trackBalance(balance, provider);
      promises.push(trackBalance);
    }

    if (isExist) {
      if (isEmptyRecord) {
        promises.push(this.pg.delTlBalanceRecord({ address, asset }, provider));
      } else {
        const record = { address, asset, data };
        promises.push(this.pg.updateBalanceRecord(record, provider));
      }
    } else {
      if (isEmptyRecord) promises.push(Promise.resolve(true));
      else promises.push(this.createBalanceRecord(balance, provider));
    }

    return Promise.all(promises).then(result => !new Set(result).has(false));
  }

  private createBalanceRecord(balance: any, provider: any): Promise<boolean> {
    const asset = balance.asset;
    return this.pg.createBalanceRecord(balance, provider).then(success => {
      if (success) this.token.updateTokenHolder({ asset, count: 1 });
      return success;
    });
  }

  private delBalanceRecord(balance: any, provider: any): Promise<boolean> {
    const { asset, address } = balance;
    return this.pg
      .delBalanceRecord({ address, asset }, provider)
      .then(success => {
        if (success) this.token.updateTokenHolder({ asset, count: -1 });
        return success;
      });
  }

  private calcQtyOwn(
    newBalance: Partial<ITokenBalance>,
    prevBalance?: Partial<PgTokenPrevBalance>,
  ): number {
    if (!prevBalance) {
      const { qty = 0, qty_in = 0 } = newBalance;
      return qty + qty_in;
    }

    const { qty, qty_in } = newBalance;
    const prevQty = +prevBalance.qty || 0;
    const prevQtyIn = +prevBalance.qty_in || 0;

    const newQty = qty === undefined ? prevQty : qty;
    const newQtyIn = qty_in === undefined ? prevQtyIn : qty_in;

    return newQty + newQtyIn;
  }

  private delInvalidAsset(assets: string[]): string[] {
    const assetsData = assets || [];
    return assetsData.filter(asset => asset !== FSN_TOKEN1);
  }
}
