import { Module } from '@nestjs/common';
import { FusionTokenModule } from './fusion-token';
import { Erc20TokenModule } from './erc20-token';
import { TokenService } from './token.service';
import { TokenClientModule, TokenClientService } from './token-client';

@Module({
  imports: [FusionTokenModule, Erc20TokenModule, TokenClientModule],
  providers: [TokenService, TokenClientService],
  exports: [
    FusionTokenModule,
    Erc20TokenModule,
    TokenClientModule,
    TokenService,
  ],
})
export class TokenModule {}
