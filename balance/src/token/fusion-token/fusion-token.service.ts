import { Injectable, HttpService, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { FSN_TOKEN } from '../../constant';
import { CustomLogger } from '../../common';
import { IAssetHolder, TokenSnapshot } from '../../models';
import { IFusionToken } from '../token.interface';

@Injectable()
export class FusionTokenService extends CustomLogger implements IFusionToken {
  constructor(private http: HttpService, private config: ConfigService) {
    super('FusionTokenService');
  }

  async getTokenBalance(holder: IAssetHolder): Promise<string> {
    const method = 'fsn_getBalance';
    const { asset, address } = holder;
    const params = [asset, address];

    return this.makeRequest(method, params);
  }

  async getTokenTlBalance(holder: IAssetHolder): Promise<any[]> {
    const method = 'fsn_getTimeLockBalance';
    const { asset, address } = holder;
    const params = [asset, address];
    return this.makeRequest(method, params).then(result => {
      if (result) return result.Items;
      return null;
    });
  }

  // Get asset's key informaton: Decimals, Symbol
  async getTokenSnapshot(token: string): Promise<TokenSnapshot> {
    if (token === FSN_TOKEN) return { symbol: 'FSN', precision: 18 };

    const method = 'fsn_getAsset';
    const tokenInfo = await this.makeRequest(method, [token]);
    const { Symbol, Decimals } = tokenInfo;

    return { symbol: Symbol, precision: Decimals };
  }

  // Fusion RPC service: https://github.com/FUSIONFoundation/efsn/wiki/FSN-RPC-API
  private async makeRequest(method: string, params: string[]): Promise<any> {
    this.logInfo({ method: 'makeRequest', data: { params } });

    const RPC_URL = this.config.get('rpc_url');
    return this.http
      .post(RPC_URL, {
        jsonrpc: '2.0',
        id: 1,
        method,
        params: [...params, 'latest'],
      })
      .toPromise()
      .then(res => res.data)
      .then(data => data.result)
      .catch(e => {
        this.logError({ method: 'makeRequest', e, data: { method, params } });
        return null;
      });
  }
}
