import { Module } from '@nestjs/common';
import { TokenClientService } from './token-client.service';
import { TOKEN_CLIENT_PROVIDER } from './token-client.provider';

@Module({
  providers: [TOKEN_CLIENT_PROVIDER, TokenClientService],
  exports: [TokenClientService, TOKEN_CLIENT_PROVIDER],
})
export class TokenClientModule {}
