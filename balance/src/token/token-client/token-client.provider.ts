import { ConfigService } from '@nestjs/config';
import { ClientProxyFactory } from '@nestjs/microservices';

export const SERVICE_NAME = 'WORKER:ASSET';

export const TOKEN_CLIENT_PROVIDER = {
  provide: SERVICE_NAME,
  inject: [ConfigService],
  useFactory: (configService: ConfigService): any => {
    const option = configService.get('worker:asset');
    return ClientProxyFactory.create(option);
  },
};
