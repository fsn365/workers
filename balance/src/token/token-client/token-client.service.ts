import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { SERVICE_NAME } from './token-client.provider';
import { CustomLogger } from '../../common';

@Injectable()
export class TokenClientService extends CustomLogger {
  constructor(@Inject(SERVICE_NAME) private client: ClientProxy) {
    super('tokenClientService');
  }

  async onApplicationBootstrap(): Promise<any> {
    await this.client.connect();
    this.logInfoMsg(`AssetClient connected....\n`);
  }

  updateTokenHolder(stats: { asset: string; count: number }): void {
    this.client.emit('holders', stats);
  }

  async trackErc20Token(token: string): Promise<void> {
    this.client.emit('erc20', { contract: token });
  }
}
