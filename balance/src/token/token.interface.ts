import { TokenSnapshot, IAssetHolder } from '../models';

export interface IToken {
  getTokenSnapshot(token: string): Promise<TokenSnapshot>;

  getTokenBalance(holder: IAssetHolder): Promise<string>;
}

export interface IFusionToken extends IToken {
  getTokenTlBalance(holder: IAssetHolder): Promise<any[]>;
}
