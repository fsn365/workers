import { Injectable } from '@nestjs/common';
import { CustomLogger } from '../common';
import { FusionTokenService } from './fusion-token';
import { Erc20TokenService } from './erc20-token';
import { RedisHelperService } from '../redis-helper';
import { TokenClientService } from './token-client';

import {
  TokenSnapshot,
  IAssetHolder,
  ITokenBalance,
  ITokenTlBalance,
  ITimeLockRecord,
} from '../models';

@Injectable()
export class TokenService extends CustomLogger {
  constructor(
    private redis: RedisHelperService,
    private erc20: Erc20TokenService,
    private fusion: FusionTokenService,
    private tokenClient: TokenClientService,
  ) {
    super('TokenService');
  }

  getTokenBalance(holder: IAssetHolder): Promise<ITokenBalance> {
    this.logInfo({ method: 'getTokenBalance', data: holder });

    let getRawBalance: Promise<string>;
    if (this.isErc20Hash(holder.asset)) {
      getRawBalance = this.erc20.getTokenBalance(holder);
      this.trackErc20Token(holder.asset);
    } else {
      getRawBalance = this.fusion.getTokenBalance(holder);
    }

    return Promise.all([getRawBalance, this.getTokenSnapshot(holder.asset)])
      .then(data => {
        const [balance, snapshot] = data;
        if (!snapshot || !balance) return null;

        const qty = this.calcTokenQty(balance, snapshot);
        if (qty) return { ...holder, qty };
        else return null;
      })
      .catch(e => {
        this.logError({ method: 'getTokenBalance', e, data: holder });
        return null;
      });
  }

  async getTokenTlBalance(holder: IAssetHolder): Promise<ITokenTlBalance> {
    this.logInfo({ method: 'getTokenTlBalance', data: holder });

    const getItems = this.fusion.getTokenTlBalance(holder);
    const getSnapshot = this.getTokenSnapshot(holder.asset);

    const { items = [], snapshot } = await Promise.all([getItems, getSnapshot])
      .then(data => {
        const [items, snapshot] = data;
        return { items, snapshot };
      })
      .catch(e => {
        this.logError({ method: 'getTokenTlBalance', e, data: holder });
        return null;
      });

    if (!items.length || !snapshot) return holder;

    const FOREVER = 18446744073709552000;
    let qty_in = -1;
    const data: ITimeLockRecord[] = [];
    items.map(({ Value, StartTime, EndTime }) => {
      const value = this.calcTokenQty(Value, snapshot.precision);
      if (EndTime === FOREVER) {
        qty_in = qty_in === -1 ? value : qty_in + value;
      }
      const record = { value, startTime: StartTime, endTime: EndTime };
      const nowTimeStamp = Date.now() / 1000;
      if (nowTimeStamp <= EndTime) data.push(record);
    });

    const balance: ITokenTlBalance = { ...holder };
    if (data.length) balance.data = data;
    if (qty_in !== -1) balance.qty_in = qty_in;

    return balance;
  }

  updateTokenHolder(stats: { asset: string; count: number }): void {
    this.tokenClient.updateTokenHolder(stats);
  }

  private async trackErc20Token(token: string): Promise<void> {
    this.tokenClient.trackErc20Token(token);
  }

  private isErc20Hash(token: string) {
    return token.length == 42;
  }

  private calcTokenQty(balance: string, token: TokenSnapshot): number {
    return +balance / Math.pow(10, token.precision);
  }

  private async getTokenSnapshot(token: string): Promise<TokenSnapshot> {
    this.logInfo({ method: 'getTokenSnapshot', data: { token } });

    const key = `token:${token}`;
    let snapshot: TokenSnapshot = await this.redis
      .getCachedValue(key)
      .then(snapshot => JSON.parse(snapshot));

    if (snapshot) return snapshot;

    if (this.isErc20Hash(token)) {
      snapshot = await this.erc20.getTokenSnapshot(token);
    } else {
      snapshot = await this.fusion.getTokenSnapshot(token);
    }

    this.redis.cacheValue(key, JSON.stringify(snapshot));

    return snapshot;
  }
}
