import { Injectable } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { PgAssetBalanceService } from './pg-asset-balance.service';
import { PgErc20BalanceService } from './pg-erc20-balance.service';
import { PgTlBalanceService } from './pg-tl-balance.service';
import { IAssetHolder } from '../models';

@Injectable()
export class PgService {
  constructor(
    @InjectKnex() private knex: Knex,
    private assetBalance: PgAssetBalanceService,
    private erc20Balance: PgErc20BalanceService,
    private tlBalance: PgTlBalanceService,
  ) {}

  getTrxProvider() {
    return this.knex.transactionProvider();
  }

  checkExistence(balance: any, provider): Promise<any> {
    const { asset, address, data } = balance;
    const holder = { asset, address };
    if (data) return this.tlBalance.checkExistence(holder, provider);
    if (this.isErc20Token(asset)) {
      return this.erc20Balance.checkExistence(holder, provider);
    }
    return this.assetBalance.checkExistence(holder, provider);
  }

  createBalanceRecord(balance: any, provider: any): Promise<boolean> {
    const { asset, data } = balance;
    if (data) this.tlBalance.createBalanceRecord(balance, provider);
    if (this.isErc20Token(asset)) {
      return this.erc20Balance.createBalanceRecord(balance, provider);
    }
    return this.assetBalance.createBalanceRecord(balance, provider);
  }

  updateBalanceRecord(balance: any, provider: any): Promise<boolean> {
    const { asset, data } = balance;
    if (data) this.tlBalance.updateBalanceRecord(balance, provider);
    if (this.isErc20Token(asset)) {
      return this.erc20Balance.updateBalanceRecord(balance, provider);
    }
    return this.assetBalance.updateBalanceRecord(balance, provider);
  }

  delBalanceRecord(balance: IAssetHolder, provider: any): Promise<boolean> {
    const { asset } = balance;
    if (this.isErc20Token(asset)) {
      return this.erc20Balance.delBalanceRecord(balance, provider);
    }
    return this.assetBalance.delBalanceRecord(balance, provider);
  }

  delTlBalanceRecord(balance: IAssetHolder, provider: any): Promise<boolean> {
    return this.tlBalance.delBalanceRecord(balance, provider);
  }

  private isErc20Token(token: string): boolean {
    return token.length === 42;
  }
}
