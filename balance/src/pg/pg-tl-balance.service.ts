import { Injectable } from '@nestjs/common';
import { PgCrud } from './pg-crud.class';
import { ITokenTlBalance } from '../models';

@Injectable()
export class PgTlBalanceService extends PgCrud {
  readonly table = 'address_tl_assets';
  readonly select = ['asset'];

  constructor() {
    super('PgTlBalanceService');
  }

  createBalanceRecord(
    balance: ITokenTlBalance,
    provider: any,
  ): Promise<boolean> {
    const { data = [], address, asset } = balance;
    const record = { address, asset, data: JSON.stringify(data) };
    return super.createBalanceRecord(record, provider);
  }

  updateBalanceRecord(
    balance: ITokenTlBalance,
    provider: any,
  ): Promise<boolean> {
    const { data, address, asset } = balance;
    const record = { address, asset, data: JSON.stringify(data) };
    return super.updateBalanceRecord(record, provider);
  }
}
