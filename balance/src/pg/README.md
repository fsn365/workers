# tables in PostgreSQL

- address_assets
  - address: string
  - asset: string
  - qty: number
  - qty_in: number
  - qty_own: number

The primary key for *address_assets* table is (address, asset);

- address_tl_assets
  - address:string
  - asset: string
  - data: json

The primary key for *address_tl_assets* table is (address, asset);

- address_erc20_assets
  - address: string
  - asset: string
  - qty: number

The primary key for *address_erc20_assets* table is (address, asset);