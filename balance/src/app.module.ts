import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';

import { config } from './config';
import { TokenModule, TokenService } from './token';
import { PgService, PgModule } from './pg';
import { RedisHelperModule } from './redis-helper';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
    }),
    HttpModule.register({
      timeout: 5000,
    }),
    PgModule,
    TokenModule,
    RedisHelperModule,
  ],
  controllers: [AppController],
  providers: [AppService, TokenService, PgService],
  exports: [AppService],
})
export class AppModule {}
