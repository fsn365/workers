export class TxTlBalanceMsg {
  s: string;
  r: string;
  assets: string[];
}

export class TxBalanceMsg {
  s: string;
  r?: string;
  assets?: string[];
}
