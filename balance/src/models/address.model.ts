export interface ITimeLockRecord {
  value: number;
  startTime: number;
  endTime: number;
}

export interface IAssetHolder {
  address: string;
  asset: string;
}

export interface ITokenBalance extends IAssetHolder {
  qty?: number;
  qty_in?: number;
  qty_own?: number;
}

export interface ITokenTlBalance extends IAssetHolder {
  data?: ITimeLockRecord[];
  qty_in?: number;
}

export interface PgTokenBalance extends ITokenBalance {
  data?: string;
}

export interface PgTokenPrevBalance extends Partial<IAssetHolder> {
  qty: number;
  qty_in: number;
  qty_own: number;
}
