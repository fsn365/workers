import { IAssetHolder } from './address.model';

export * from './token.model';
export * from './msg.model';
export * from './address.model';

export interface BalancePair extends IAssetHolder {
  type: string;
}

export interface MsgBalancePair {
  address: string;
  assets?: string[];
}
