import { Controller, Logger } from '@nestjs/common';
import {
  Ctx,
  Payload,
  RmqContext,
  MessagePattern,
} from '@nestjs/microservices';
import { AppService } from './app.service';
import { TxBalanceMsg, TxTlBalanceMsg } from './models';

@Controller()
export class AppController {
  private logger = new Logger(`AppController`);

  constructor(private readonly service: AppService) {}

  @MessagePattern('balance')
  updateAddressBalance(
    @Payload() msg: TxBalanceMsg,
    @Ctx() ctx: RmqContext,
  ): void {
    this.service.processTxBalanceMsg(msg).then(res => {
      if (res) this.ackMsg(ctx);
    });
  }

  @MessagePattern('tl_balance')
  updateAddressTlBalance(
    @Payload() msg: TxTlBalanceMsg,
    @Ctx() ctx: RmqContext,
  ): void {
    this.service.processTlTxBalanceMsg(msg).then(res => {
      if (res) this.ackMsg(ctx);
    });
  }

  private ackMsg(ctx: RmqContext) {
    const rawMsg = ctx.getMessage();
    const channel = ctx.getChannelRef();
    channel.ack(rawMsg);

    const msg = JSON.parse(rawMsg.content.toString());
    this.logger.log(`Ack ${msg.pattern}, data:`);
    this.logger.verbose(msg.data);
  }
}
