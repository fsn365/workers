# Address Services

## How to start services

- npm install
- npm run build
- config *.env* file(please follow sample.env)
- pm2 start ecosystem

It will start a cluster of process at max value.

## Functionality

- update address information

- update address timelock fusion assets balance

- update address fusion asset balance

## Events

- **address**: create or update address record

- **adress:balance**:create or update address balance for a fusion asset

- **adderss:tl_balance**: create or update address time lock balance for fusion asset

## Influenced tables

- address

- address_assets

- address_tl_assets

