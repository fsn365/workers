import { Injectable } from '@nestjs/common';
import { RedisService } from 'nestjs-redis';

@Injectable()
export class RedisHelperService {
  private redis: any;

  constructor(redisService: RedisService) {
    this.redis = redisService.getClient('swap');
  }

  cacheValue(key: string, value: string): void {
    const redisKey = this.getKey(key);
    this.redis.set(redisKey, value);
  }

  getCachedValue(key: string): Promise<string | null> {
    const redisKey = this.getKey(key);
    return this.redis.get(redisKey);
  }

  private getKey(key: string): string {
    return `worker:tx:${key}`;
  }
}
