import { Module, HttpModule, Global } from '@nestjs/common';
import { HelperService } from './helper.service';
import { RedisHelperModule } from './redis-helper';
import { RedisHelperService } from './redis-helper';
import { RpcHelperService } from './rpc-helper.service';

@Global()
@Module({
  imports: [RedisHelperModule, HttpModule.register({})],
  providers: [HelperService, RedisHelperService, RpcHelperService],
})
export class HelperModule {}
