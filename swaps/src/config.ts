import { Transport } from '@nestjs/microservices';
import { Web3Server } from './web3-server';

export const config = () => ({
  postgres: {
    connection: {
      host: process.env.db_host,
      port: process.env.db_port,
      user: process.env.db_username,
      password: process.env.db_password,
      database: process.env.db_name,
    },
    searchPath: ['public'],
    client: 'pg',
    ssl: true,
  },

  redis: {
    name: 'swap',
    host: process.env.redis_host || 'localhost',
    port: process.env.redis_port || 6379,
  },

  app: {
    name: 'swap worker',
    transport: Transport.TCP,
    options: {
      host: process.env.app_host,
      port: process.env.app_port,
    },
  },

  rpc: process.env.rpc_url,

  mongodb: {
    uri: process.env['anyswap_mongo_uri'],
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },

  ['service:web3']: {
    strategy: new Web3Server(process.env.web3_wss_url),
  },
});
