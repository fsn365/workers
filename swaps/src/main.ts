import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { config } from './config';

async function bootstrap() {
  const CONFIG = config();
  const { name, ...options } = CONFIG.app;

  const app = await NestFactory.createMicroservice(options);
  const logger = new Logger(name);

  const msg = `\n\n
=========================================================
        ${name} started.
=========================================================
\n\n
    `;
  await app.listen(() => {
    logger.log(msg);
  });
}
bootstrap();
