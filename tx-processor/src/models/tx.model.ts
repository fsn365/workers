export class TxBase {
  hash: string;
  status: number;
  block: number;
  timestamp: number;
}

export class RawTx extends TxBase {
  gasLimit: number;
  gasUsed: number;
  from: string;
  to: string;
  type: string;
  ivalue: string;
  dvalue: string;
  log: any;
  erc20Receipts?: any;
  exchangeReceipts?: any;
}

export class ProcessedTx extends TxBase {
  fee: number;
  sender: string;
  receiver: string;
  type: number;
  assets?: string[];
  data?: any;
}

export class PgTx extends TxBase {
  assets?: string;
  data?: string;
}

export class TxAssetData {
  asset: string;
  symbol: string;
  value: number;
}

export class SendAssetTxData extends TxAssetData {}

export class AssetChangeTxData extends TxAssetData {
  isInc: boolean;
}

export class TimeLockTxsData extends SendAssetTxData {
  startTime: number;
  endTime: number;
  type: string;
}

export class TxAssetsAndData<T> {
  data?: T;
  assets?: string[];
}

export class TxsRange {
  $lte: number;
  $gt: number;
}

export class RangeRawTxs {
  syncHeight: number;
  rawTxs: RawTx[];
}

export class RangeTxsStats {
  stats_at: number;
  stats: {
    txs: number;
    ticket_txs: number;
  };
}
