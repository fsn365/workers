export const TRANSACTION_TYPES = {
  unknown: { type: 'unknown', id: -1 },
  GenNotationFunc: { type: 'GenNotationFunc', id: 0 },
  GenAssetFunc: { type: 'GenAssetFunc', id: 1 },
  SendAssetFunc: { type: 'SendAssetFunc', id: 2 },
  TimeLockFunc: { type: 'TimeLokcFunc', id: 3 },
  OldAssetValueChangeFunc: { type: 'OldAssetValueChangeFunc', id: 4 },
  AssetValueChangeFunc: { type: 'AssetValueChangeFunc', id: 4 },
  MakeSwapFunc: { type: 'MakeSwapFunc', id: 5 },
  RecallSwapFunc: { type: 'RecallSwapFunc', id: 6 },
  TakeSwapFunc: { type: 'TakeSwapFunc', id: 7 },
  EmptyFunc: { type: 'EmptyFunc', id: 8 },
  MakeSwapFuncExt: { type: 'MakeSwapFuncExt', id: 9 },
  TakeSwapFuncExt: { type: 'TakeSwapFuncExt', id: 10 },
  MakeMultiSwapFunc: { type: 'MakeMultipleSwapFunc', id: 11 },
  RecallMultiSwapFunc: { type: 'RecallMultiSwapFunc', id: 12 },
  TakeMultiSwapFunc: { type: 'TakeMultiSwapFunc', id: 13 },
  ReportIllegalFunc: { type: 'ReportIllegalFunc', id: 14 },
  TokenPurchase: { type: 'TokenPurchase', id: 15 },
  EthPurchase: { type: 'EthPurchase', id: 16 },
  AddLiquidity: { type: 'AddLiquidity', id: 17 },
  RemoveLiquidity: { type: 'RemoveLiquidity', id: 18 },
  Approval: { type: 'Approval', id: 19 },
  CreateContract: { type: 'CreateContract', id: 20 },
  Transfer: { type: 'Transfer', id: 21 },
  DexSwap: { type: 'AnySwap', id: 22 },
};

export const SWAP_TYPES = [
  'MakeSwapFunc',
  'RecallSwapFunc',
  'TakeSwapFunc',
  'MakeSwapFuncExt',
  'TakeSwapFuncExt',
  'MakeMultiSwapFunc',
  'RecallMultiSwapFunc',
  'TakeMultiSwapFunc',
];

export const NONE_NATIVE_TRANSACTION_TYPES = [
  'ERC20',
  'Origin',
  'MBTC',
  'unknown',
];

export const FSN_TOKEN =
  '0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff';

export const FSN_CONTRACT = '0xffffffffffffffffffffffffffffffffffffffff';

export const SMART_CONTRACT_TIMELOCKFUNCS = [
  'TimeLockContractSend',
  'TimeLockContractReceive',
];
