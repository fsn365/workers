import { Injectable } from '@nestjs/common';
import { HelperService } from './helper';
import { TaskService } from './task';

@Injectable()
export class AppService {
  constructor(private task: TaskService, private helper: HelperService) {}

  async makeNetworkTxsStats(blockData: any): Promise<void> {
    const { number, timestamp } = blockData;
    this.helper.updateNetworkState({ number, timestamp });

    const prevTrackTxsStatsAt = await this.task.getTxsStatsPrevTrackAt();
    const ONE_DAY = 86400;
    if (ONE_DAY + prevTrackTxsStatsAt <= timestamp) {
      this.task.statsTxs();
    }
  }
}
