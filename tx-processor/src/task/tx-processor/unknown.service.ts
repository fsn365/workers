import { Injectable } from '@nestjs/common';
import { TxProcessor } from './tx-processor';
import { RawTx, TxAssetsAndData } from '../../models';
import { WorkerClientService } from '../worker-client';
import { TokenService } from './token';

@Injectable()
export class UnknownService {
  constructor(private workerClient: WorkerClientService) {}

  async getTxsAssetsAndData(
    rawTx: RawTx,
    tokenService: TokenService,
  ): Promise<TxAssetsAndData<any>> {
    console.log(rawTx.type, rawTx.hash);
    return TxProcessor.getTxsAssetsAndDataForUnknowTx(
      rawTx,
      tokenService,
      this.workerClient,
    );
  }
}
