import { Injectable } from '@nestjs/common';
import { Erc20TokenService } from './erc20-token/erc20-token.service';
import { FusionTokenService } from './fusion-token.service';
import { RedisHelperService } from '../../../helper';
import { TokenSnapshot } from '../../../models';
import { FSN_TOKEN } from '../../../common';

@Injectable()
export class TokenService {
  constructor(
    private redis: RedisHelperService,
    private fusion: FusionTokenService,
    private erc20: Erc20TokenService,
  ) {}

  async getTokenSnapshot(token: string): Promise<TokenSnapshot> {
    if (token === FSN_TOKEN) return { symbol: 'FSN', precision: 18 };

    let snapshot: TokenSnapshot | null = await this.redis
      .getCachedValue(token)
      .then(val => JSON.parse(val));

    if (snapshot) return snapshot;

    if (token.length == 42) snapshot = await this.erc20.getTokenSnapshot(token);
    else snapshot = await this.fusion.getTokenSnapshot(token);

    if (snapshot) this.redis.cacheValue(token, snapshot);

    return snapshot;
  }
}
