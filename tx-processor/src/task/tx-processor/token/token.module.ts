import { Module } from '@nestjs/common';
import { Erc20TokenModule } from './erc20-token/erc20-token.module';
import { Erc20TokenService } from './erc20-token/erc20-token.service';
import { TokenService } from './token.service';
import { FusionTokenService } from './fusion-token.service';

@Module({
  imports: [Erc20TokenModule],
  providers: [TokenService, FusionTokenService, Erc20TokenService],
  exports: [TokenService, FusionTokenService, Erc20TokenService],
})
export class TokenModule {}
