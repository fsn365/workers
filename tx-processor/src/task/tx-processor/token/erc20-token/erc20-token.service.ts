import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TokenSnapshot } from '../../../../models';
import { CustomLogger } from '../../../../common';
import { abi } from './abi';

const Web3 = require('web3');

@Injectable()
export class Erc20TokenService extends CustomLogger {
  private web3: any;

  constructor(config: ConfigService) {
    super('Erc20TokenService');
    const RPC_PROVIDER = config.get('rpc_url');
    this.web3 = new Web3(RPC_PROVIDER);
  }

  async getTokenSnapshot(address: string): Promise<TokenSnapshot> {
    let contract = new this.web3.eth.Contract(abi, address);
    const props = ['symbol', 'decimals'];
    const promises = props.map(prop => this.getErc20Prop(contract, prop));

    const snapshot = await Promise.all(promises)
      .then(data => {
        const [symbol, precision] = data;
        return { symbol, precision };
      })
      .catch(e => {
        this.logError({ method: 'getTokenSnapshot', e });
        return { symbol: 'unknwon', precision: 18 };
      });

    contract = null;

    return snapshot;
  }

  private getErc20Prop(contract: any, prop: string): Promise<any> {
    return new Promise((resolve, reject) => {
      contract.methods[prop]().call((err, res) => {
        if (err) reject(err);
        if (res) resolve(res);
      });
    });
  }
}
