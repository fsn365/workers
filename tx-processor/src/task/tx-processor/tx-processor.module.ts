import { Module } from '@nestjs/common';
import { TxProcessorService } from './tx-processor.service';
import { AssetChangeService } from './asset-change.service';
import { GenAssetService } from './gen-asset.service';
import { GenNotationService } from './gen-notation.service';
import { SendAssetService } from './send-asset.service';
import { SwapService } from './swap.service';
import { OriginService } from './origin.service';
import { UnknownService } from './unknown.service';
import { TimelockAssetService } from './timelock-asset.service';
import { TokenModule } from './token/token.module';
import { TokenService } from './token/token.service';
import { Erc20Service } from './erc20.service';
import { WorkerClientModule } from '../worker-client/worker-client.module';
import { MbtcService } from './mbtc.service';

@Module({
  imports: [TokenModule, WorkerClientModule],
  providers: [
    SendAssetService,
    GenNotationService,
    GenAssetService,
    AssetChangeService,
    SwapService,
    OriginService,
    UnknownService,
    Erc20Service,
    TxProcessorService,
    TimelockAssetService,
    TokenService,
    SendAssetService,
    MbtcService,
  ],
  exports: [
    TxProcessorService,
    GenNotationService,
    GenAssetService,
    AssetChangeService,
    SwapService,
    OriginService,
    UnknownService,
    Erc20Service,
    TxProcessorService,
    TimelockAssetService,
    TokenService,
    SendAssetService,
    MbtcService,
  ],
})
export class TxProcessorModule {}
