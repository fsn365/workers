import { Injectable } from '@nestjs/common';
import { RawTx, TimeLockTxsData, TxAssetsAndData } from '../../models';
import { TokenService } from './token';

@Injectable()
export class TimelockAssetService {
  async getTxsAssetsAndData(
    rawTx: RawTx,
    tokenService: TokenService,
  ): Promise<TxAssetsAndData<TimeLockTxsData>> {
    if (!rawTx.log) {
      return { assets: [], data: null };
    }
    const { log } = rawTx;

    const { AssetID, Value, StartTime, EndTime, LockType } = log;
    const { symbol, precision } = await tokenService.getTokenSnapshot(AssetID);
    const value = +Value / Math.pow(10, precision);

    const data = {
      value,
      asset: AssetID,
      symbol,
      startTime: StartTime,
      endTime: EndTime,
      type: LockType,
    };

    const assets = [rawTx.log.AssetID];

    return { data, assets };
  }
}
