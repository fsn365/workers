import { Injectable } from '@nestjs/common';
import { SendAssetService } from './send-asset.service';
import { TimelockAssetService } from './timelock-asset.service';
import { ProcessedTx, RawTx, TxAssetsAndData } from '../../models';
import { GenAssetService } from './gen-asset.service';
import { AssetChangeService } from './asset-change.service';
import { GenNotationService } from './gen-notation.service';
import { SwapService } from './swap.service';
import { OriginService } from './origin.service';
import { TokenService } from './token';
import { UnknownService } from './unknown.service';
import { Erc20Service } from './erc20.service';
import { TxProcessor } from './tx-processor';
import { CustomLogger } from '../../common';
import { MbtcService } from './mbtc.service';

@Injectable()
export class TxProcessorService extends CustomLogger {
  constructor(
    private sendAsset: SendAssetService,
    private timelock: TimelockAssetService,
    private genAsset: GenAssetService,
    private assetChange: AssetChangeService,
    private genNotation: GenNotationService,
    private swap: SwapService,
    private origin: OriginService,
    private unknown: UnknownService,
    private erc20: Erc20Service,
    private mbtc: MbtcService,
    private tokenService: TokenService,
  ) {
    super('TxProcessorService');
  }

  async processTxs(rawTxs: RawTx[]): Promise<ProcessedTx[]> {
    const startAt = Date.now();

    const promises = rawTxs.map(rawTx => this.processTx(rawTx));
    const txs = await Promise.all(promises);

    const cost = Date.now() - startAt;
    this.logInfoMsg(`Processed ${txs.length} txs, Cost ${cost} ms.`);

    return this.sortTxs(txs);
  }

  private async processTx(rawTx: RawTx): Promise<ProcessedTx> {
    const { assets = [], data = null } = await this.getTxsAssetAndData(rawTx);
    const tx: ProcessedTx = TxProcessor.cleanTx(rawTx);

    if (assets.length) tx.assets = assets;
    if (data) tx.data = data;

    return tx;
  }

  private async getTxsAssetAndData(
    rawTx: RawTx,
  ): Promise<TxAssetsAndData<any>> {
    const { type, hash, log } = rawTx;
    // this.logInfo({ method: type, data: rawTx  });
    switch (type) {
      case 'SendAssetFunc':
        return this.sendAsset.getTxsAssetsAndData(rawTx, this.tokenService);
      case 'TimeLockFunc':
        return this.timelock.getTxsAssetsAndData(rawTx, this.tokenService);
      case 'GenAssetFunc':
        return this.genAsset.getTxsAssetsAndData(rawTx);
      case 'AssetValueChangeFunc':
      case 'OldAssetValueChangeFunc':
        return this.assetChange.getTxsAssetsAndData(rawTx, this.tokenService);
      case 'GenNotationFunc':
        return this.genNotation.getTxsAssetsAndData(rawTx);
      case 'MakeSwapFunc':
      case 'RecallSwapFunc':
      case 'TakeSwapFunc':
      case 'MakeSwapFuncExt':
      case 'TakeSwapFuncExt':
      case 'MakeMultiSwapFunc':
      case 'RecallMultiSwapFunc':
      case 'TakeMultiSwapFunc':
        return this.swap.getTxsAssetsAndData(rawTx);
      case 'ERC20':
        return this.erc20.getTxsAssetsAndData(rawTx, this.tokenService);
      case 'MBTC':
        return this.mbtc.getTxsAssetsAndData(rawTx, this.tokenService);
      case 'Origin':
        return this.origin.getTxsAssetsAndData(rawTx, this.tokenService);
      case 'ReportIllegalFunc':
      case 'unknown':
      default:
        return this.unknown.getTxsAssetsAndData(rawTx, this.tokenService);
    }
  }

  private sortTxs(txs: ProcessedTx[]): ProcessedTx[] {
    return txs.sort((tx1, tx2) => tx1.block - tx2.block);
  }
}
