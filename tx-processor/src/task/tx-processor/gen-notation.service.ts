import { Injectable } from '@nestjs/common';
import { RawTx, TxAssetsAndData } from '../../models';

@Injectable()
export class GenNotationService {
  async getTxsAssetsAndData(
    rawTx: RawTx,
  ): Promise<TxAssetsAndData<{ usan: number }>> {
    if (!rawTx.log) {
      return { data: null, assets: [] };
    }
    const data = { usan: rawTx.log.notation };
    return { data };
  }
}
