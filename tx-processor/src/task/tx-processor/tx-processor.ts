import { ProcessedTx, RawTx, TxAssetsAndData } from '../../models';
import { TokenService } from './token/token.service';
import { WorkerClientService } from '../worker-client';

import {
  TRANSACTION_TYPES,
  FSN_CONTRACT,
  FSN_TOKEN,
  SMART_CONTRACT_TIMELOCKFUNCS,
} from '../../common';

export abstract class TxProcessor {
  static cleanTx(rawTx: RawTx): ProcessedTx {
    const { gasLimit, gasUsed, from, ...tx } = rawTx;
    const txData = { ...tx };

    delete txData.ivalue;
    delete txData.dvalue;
    delete txData.erc20Receipts;
    delete txData.exchangeReceipts;
    delete txData.log;
    delete txData.type;
    delete txData.to;

    const fee = (gasLimit * gasUsed) / Math.pow(10, 18);

    const sender = from;
    const receiver = TxProcessor.getTxsRecevier(rawTx);
    const type = TxProcessor.getTxsTypeID(rawTx);

    return { ...txData, type, sender, receiver, fee };
  }

  static getTxsRecevier(rawTx: RawTx): string {
    const { to, type, erc20Receipts = [], exchangeReceipts = [] } = rawTx;

    switch (type) {
      case TRANSACTION_TYPES['SendAssetFunc'].type:
      case TRANSACTION_TYPES['TimeLockFunc'].type:
        return to;
      case TRANSACTION_TYPES['GenAssetFunc'].type:
      case TRANSACTION_TYPES['AssetValueChangeFunc'].type:
      case TRANSACTION_TYPES['GenNotationFunc'].type:
      case TRANSACTION_TYPES['ReportIllegalFunc'].type:
      case TRANSACTION_TYPES['MakeSwapFunc'].type:
      case TRANSACTION_TYPES['RecallSwapFunc'].type:
      case TRANSACTION_TYPES['TakeSwapFunc'].type:
      case TRANSACTION_TYPES['MakeSwapFuncExt'].type:
      case TRANSACTION_TYPES['TakeMultiSwapFunc'].type:
      case TRANSACTION_TYPES['RecallMultiSwapFunc'].type:
      case TRANSACTION_TYPES['TakeMultiSwapFunc'].type:
      case TRANSACTION_TYPES['ReportIllegalFunc'].type:
      case TRANSACTION_TYPES['CreateContract'].type:
        return FSN_CONTRACT;
      case 'ERC20':
        if (rawTx.log) return rawTx.erc20Receipts[0].to;
        else return rawTx.to;
      case 'Origin':
      case 'unknown':
      default: {
        if (!erc20Receipts.length) {
          return TxProcessor.getTxsRecevierForUnkownTx(rawTx);
        }
        if (exchangeReceipts.length) return rawTx.to;
        else return erc20Receipts[0].to;
      }
    }
  }

  static getTxsTypeID(rawTx: RawTx): number {
    const { log, erc20Receipts, exchangeReceipts } = rawTx;
    let type: number;

    if (rawTx.type !== 'unknown' && TRANSACTION_TYPES[rawTx.type]) {
      return TRANSACTION_TYPES[rawTx.type].id;
    }

    // log is null
    if (!log) return TRANSACTION_TYPES['SendAssetFunc'].id;

    const isContractLog = Array.isArray(log);

    if (!isContractLog) return TRANSACTION_TYPES['unknown'].id;

    // log is emited from unkown smart contract
    if (!erc20Receipts && isContractLog) {
      log.map((logItem: any) => {
        const { topic } = logItem;
        if (SMART_CONTRACT_TIMELOCKFUNCS.includes(topic))
          type = TRANSACTION_TYPES['TimeLockFunc'].id;
        else type = TRANSACTION_TYPES['unknown'].id;
      });
      return type;
    }

    // log is erc20 log
    if (!exchangeReceipts) {
      const logType = erc20Receipts[0].logType;
      return TRANSACTION_TYPES[logType].id;
    }

    // log is exchange trading log for fsn to any erc20 token
    if (exchangeReceipts.length === 1) {
      const txnsType = exchangeReceipts[0].txnsType;
      return TRANSACTION_TYPES[txnsType].id;
    }

    // log is erc20 trading pair's log
    return TRANSACTION_TYPES['DexSwap'].id;
  }

  static async getTxsAssetsAndDataForUnknowTx(
    rawTx: RawTx,
    tokenService: TokenService,
    workerClient: WorkerClientService,
  ): Promise<TxAssetsAndData<any>> {
    const { log, ivalue, dvalue, erc20Receipts } = rawTx;
    const isContractLog = Array.isArray(log);

    let data: any = null;
    let assets: string[] = [];

    if (!log || (log && !isContractLog)) {
      let value = +ivalue + +dvalue / Math.pow(10, 18);
      data = { symbol: 'FSN', value, asset: FSN_TOKEN };
      assets.push(FSN_TOKEN);
    }

    if (isContractLog && !erc20Receipts) {
      log.map(async (logItem: any) => {
        const { contract, topic, asset, start, end, value } = logItem;

        if (contract) {
          workerClient.notifyAddressInfo({ address: contract, contract: true });
        }

        if (SMART_CONTRACT_TIMELOCKFUNCS.includes(topic)) {
          const tokenSnapshot = await tokenService.getTokenSnapshot(asset);
          assets.push(asset);
          const { symbol, precision } = tokenSnapshot;
          const qty = value / Math.pow(10, precision);
          data = {
            symbol,
            asset,
            value: qty,
            startTime: +start,
            endTime: +end,
            lockType: topic,
          };
        } else {
          data = log;
          assets.push(FSN_TOKEN);
        }
      });
    }

    if (erc20Receipts) {
      const { erc20, value } = erc20Receipts[0];
      const tokenSnapshot = await tokenService.getTokenSnapshot(erc20);
      const { symbol, precision } = tokenSnapshot;
      const qty = +value / Math.pow(10, precision);

      data = { value: qty, asset: erc20, symbol };
      assets.push(erc20);
    }

    return { assets, data };
  }

  static getTxsRecevierForUnkownTx(rawTx: RawTx): string {
    const { log } = rawTx;

    // log is null
    if (!log) return rawTx.to;

    const isContractLog = Array.isArray(log);

    // log is object
    if (!isContractLog) {
      const receiver = rawTx.to;
      if (receiver === FSN_CONTRACT) return rawTx.log.To || rawTx.to;
      else return rawTx.to;
    }

    // log is unknow smart contract's log
    if (isContractLog) {
      let receiver: string;
      rawTx.log.map((logItem: any) => {
        receiver = logItem.to;
      });

      return receiver || rawTx.to;
    }
  }
}
