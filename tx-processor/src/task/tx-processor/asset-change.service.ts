import { Injectable } from '@nestjs/common';
import { RawTx, AssetChangeTxData, TxAssetsAndData } from '../../models';
import { TokenService } from './token';

@Injectable()
export class AssetChangeService {
  async getTxsAssetsAndData(
    rawTx: RawTx,
    tokenService: TokenService,
  ): Promise<TxAssetsAndData<AssetChangeTxData>> {
    const { IsInc, AssetID, Value } = rawTx.log;
    const snapshot = await tokenService.getTokenSnapshot(AssetID);
    const { symbol, precision } = snapshot;
    const value = Value / Math.pow(10, precision);

    const data = { isInc: IsInc, asset: AssetID, symbol, value };
    const assets = [AssetID];

    return { data, assets };
  }
}
