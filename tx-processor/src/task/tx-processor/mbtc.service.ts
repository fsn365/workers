import { Injectable } from '@nestjs/common';
import { RawTx, TxAssetData, TxAssetsAndData } from '../../models';
import { TokenService } from './token';

@Injectable()
export class MbtcService {
  async getTxsAssetsAndData(
    rawTx: RawTx,
    tokenService: TokenService,
  ): Promise<TxAssetsAndData<TxAssetData>> {
    const { erc20, value } = rawTx.erc20Receipts[0];
    const tokenSnapshot = await tokenService.getTokenSnapshot(erc20);
    const { symbol, precision } = tokenSnapshot;
    const qty = +value / Math.pow(10, precision);

    const data = { value: qty, asset: erc20, symbol };
    const assets = [erc20];

    return { data, assets };
  }
}
