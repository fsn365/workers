import { Injectable } from '@nestjs/common';
import { RawTx, TxAssetsAndData } from '../../models';
import { RpcHelperService } from '../../helper';

@Injectable()
export class SwapService {
  constructor(private rpc: RpcHelperService) {}

  async getTxsAssetsAndData(
    rawTx: RawTx,
  ): Promise<TxAssetsAndData<{ swap: string }>> {
    const { type, log } = rawTx;
    const swap = log.SwapID;
    const data = { swap };

    let assets: string[];
    if (type.indexOf('Take') > -1 || type.indexOf('Recall') > -1) {
      const { FromAssetID, ToAssetID } = await this.getSwapedAssets(swap, type);
      assets = Array.from(new Set([...FromAssetID, ...ToAssetID]));
    }

    const { FromAssetID, ToAssetID } = log;
    if (type === 'MakeMultiSwapFunc') {
      assets = Array.from(new Set([...FromAssetID, ...ToAssetID]));
    }
    if (type === 'MakeSwapFuncExt' || type === 'MakeSwapFunc') {
      assets = Array.from(new Set([FromAssetID, ToAssetID]));
    }

    return { data, assets };
  }

  private async getSwapedAssets(
    swapID: string,
    type: string,
  ): Promise<{ FromAssetID: string[]; ToAssetID: string[] }> {
    const SWAP_TYPES_METHOD = {
      TakeSwapFunc: 'fsn_getSwap',
      RecallSwapFunc: 'fsn_getSwap',
      TakeMultiSwapFunc: 'fsn_getMultiSwap',
      RecallMultiSwapFunc: 'fsn_getMultiSwap',
    };

    const result = { FromAssetID: [], ToAssetID: [] };

    if (!SWAP_TYPES_METHOD[type]) return result;

    const method = SWAP_TYPES_METHOD[type];
    const params = [swapID];
    return this.rpc
      .makeRequest({ method, params })
      .then(resultData => {
        if (!resultData) return result;
        const { FromAssetID, ToAssetID } = resultData;
        if (method === 'fsn_getSwap') {
          result.FromAssetID.push(FromAssetID);
          result.ToAssetID.push(ToAssetID);
        }
        if (method === 'fsn_getMultiSwap') {
          result.FromAssetID = FromAssetID;
          result.ToAssetID = ToAssetID;
        }
        return result;
      })
      .catch(e => {
        // this.logError({
        //   method: 'getSwappedAssets',
        //   e,
        //   data: { params, method },
        // });
        return result;
      });
  }
}
