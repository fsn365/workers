import { Injectable } from '@nestjs/common';
import { RawTx, TxAssetsAndData, TxAssetData } from '../../models';
import { TokenService } from './token';

@Injectable()
export class SendAssetService {
  async getTxsAssetsAndData(
    rawTx: RawTx,
    tokenService: TokenService,
  ): Promise<TxAssetsAndData<TxAssetData>> {
    if (!rawTx.log) {
      return { data: null, assets: [] };
    }
    const { AssetID, Value } = rawTx.log;
    const snapshot = await tokenService.getTokenSnapshot(AssetID);
    const { symbol, precision } = snapshot;
    const value = +Value / Math.pow(10, precision);

    const data = { symbol, value, asset: AssetID };
    const assets = [AssetID];

    return { data, assets };
  }
}
