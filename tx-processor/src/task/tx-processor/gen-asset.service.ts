import { Injectable } from '@nestjs/common';
import { RawTx, TxAssetData, TxAssetsAndData } from '../../models';

@Injectable()
export class GenAssetService {
  async getTxsAssetsAndData(
    rawTx: RawTx,
  ): Promise<TxAssetsAndData<TxAssetData>> {
    const { AssetID, Total, Symbol, Decimals } = rawTx.log;
    const value = +Total / Math.pow(10, Decimals);

    const data = { symbol: Symbol, value, asset: AssetID };
    const assets = [AssetID];

    return { data, assets };
  }
}
