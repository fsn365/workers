import { Injectable } from '@nestjs/common';
import { TxProcessor } from './tx-processor';
import { RawTx, TxAssetsAndData, TxAssetData } from '../../models';
import { TokenService } from './token/token.service';
import { WorkerClientService } from '../worker-client/worker-client.service';
import { TRANSACTION_TYPES, FSN_TOKEN } from '../../common';

@Injectable()
export class OriginService {
  constructor(private workerClient: WorkerClientService) {}

  getTxsAssetsAndData(
    rawTx: RawTx,
    tokenService: TokenService,
  ): Promise<TxAssetsAndData<any>> {
    const { exchangeReceipts = [], erc20Receipts = [] } = rawTx;

    const isUnknwonTx = erc20Receipts.length === 0;
    const isErc20Transfer = !exchangeReceipts.length && erc20Receipts.length;
    const isFsnTradingPair = exchangeReceipts.length === 1;
    const isErc20TradingPair = exchangeReceipts.length === 2;

    if (isUnknwonTx)
      return TxProcessor.getTxsAssetsAndDataForUnknowTx(
        rawTx,
        tokenService,
        this.workerClient,
      );
    if (isErc20Transfer) {
      return this.processErc20Transfer(rawTx, tokenService);
    }
    if (isFsnTradingPair) {
      return this.processFsnTradingPair(rawTx, tokenService);
    }
    if (isErc20TradingPair) {
      return this.processErc20TradingPair(rawTx, tokenService);
    }
  }

  private async processFsnTradingPair(
    rawTx: RawTx,
    tokenService: TokenService,
  ): Promise<TxAssetsAndData<{ from: TxAssetData; to: TxAssetData }>> {
    const { erc20Receipts, exchangeReceipts } = rawTx;

    const { erc20 } = erc20Receipts[0];
    const { txnsType } = exchangeReceipts[0];
    const erc20TokenSnapshot = await tokenService.getTokenSnapshot(erc20);
    const fsnSnapshot = { symbol: 'FSN', precision: 18 };

    let fromToken = null;
    let toToken = null;
    let fromAsset: string;
    let toAsset: string;

    if (txnsType === TRANSACTION_TYPES['EthPurchase'].type) {
      fromToken = erc20TokenSnapshot;
      toToken = fsnSnapshot;
      fromAsset = erc20;
      toAsset = FSN_TOKEN;
    }

    // current only support fsn pair
    if (
      txnsType === TRANSACTION_TYPES['TokenPurchase'].type ||
      txnsType === TRANSACTION_TYPES['AddLiquidity'].type ||
      txnsType === TRANSACTION_TYPES['RemoveLiquidity'].type
    ) {
      fromToken = fsnSnapshot;
      toToken = erc20TokenSnapshot;
      fromAsset = FSN_TOKEN;
      toAsset = erc20;
    }

    const { tokenFromAmount, tokenToAmount } = exchangeReceipts[0];
    const fromQty = +tokenFromAmount / Math.pow(10, fromToken.precision);
    const toQty = +tokenToAmount / Math.pow(10, toToken.precision);
    const data = {
      from: {
        asset: fromAsset,
        symbol: fromToken.symbol,
        value: fromQty,
      },
      to: {
        asset: toAsset,
        symbol: toToken.symbol,
        value: toQty,
      },
    };

    return { assets: [fromAsset, toAsset], data };
  }

  private async processErc20TradingPair(
    rawTx: RawTx,
    tokenService: TokenService,
  ): Promise<TxAssetsAndData<{ from: TxAssetData; to: TxAssetData }>> {
    const { erc20Receipts, exchangeReceipts } = rawTx;

    const len = erc20Receipts.length;

    const fromAsset = erc20Receipts[0].erc20;
    const toAsset = erc20Receipts[len - 1].erc20;

    const [fromTokenSnapshot, toTokenSnapshot] = await Promise.all([
      tokenService.getTokenSnapshot(fromAsset),
      tokenService.getTokenSnapshot(toAsset),
    ]);

    const tokenFromAmount = exchangeReceipts[1].tokenFromAmount;
    const tokenToAmount = exchangeReceipts[0].tokenToAmount;

    const fromQty = tokenFromAmount / Math.pow(10, fromTokenSnapshot.precision);
    const toqQty = tokenToAmount / Math.pow(10, toTokenSnapshot.precision);

    const data = {
      from: {
        asset: fromAsset,
        symbol: fromTokenSnapshot.symbol,
        value: fromQty,
      },
      to: {
        asset: toAsset,
        symbol: toTokenSnapshot.symbol,
        value: toqQty,
      },
    };

    return { assets: [fromAsset, toAsset], data };
  }

  private async processErc20Transfer(
    rawTx: RawTx,
    tokenService: TokenService,
  ): Promise<TxAssetsAndData<TxAssetData>> {
    const { erc20Receipts } = rawTx;
    const { erc20, value } = erc20Receipts[0];
    const tokenSnapshot = await tokenService.getTokenSnapshot(erc20);
    const { symbol, precision } = tokenSnapshot;
    const qty = +value / Math.pow(10, precision);

    const data = { value: qty, asset: erc20, symbol };
    const assets = [erc20];

    return { data, assets };
  }
}
