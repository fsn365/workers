import { Injectable } from '@nestjs/common';
import { RawTx, TxAssetData, TxAssetsAndData } from '../../models';
import { TokenService } from './token';
import { FSN_TOKEN } from '../../common';

@Injectable()
export class Erc20Service {
  async getTxsAssetsAndData(
    rawTx: RawTx,
    tokenService: TokenService,
  ): Promise<TxAssetsAndData<TxAssetData>> {
    const { log, erc20Receipts = [], ivalue, dvalue } = rawTx;
    if (!log) {
      const value = +ivalue + +dvalue / Math.pow(10, 18);
      const data = { asset: FSN_TOKEN, symbol: 'FSN', value };
      return { data, assets: [FSN_TOKEN] };
    }
    const { erc20, value } = erc20Receipts[0];
    const tokenSnapshot = await tokenService.getTokenSnapshot(erc20);
    const { symbol, precision } = tokenSnapshot;
    const qty = +value / Math.pow(10, precision);

    const data = { value: qty, asset: erc20, symbol };
    const assets = [erc20];

    return { data, assets };
  }
}
