import { Injectable } from '@nestjs/common';
import { PgService } from './pg';
import { TxProcessorService } from './tx-processor';
import { MongoService } from './mongo/mongo.service';
import { CustomLogger } from '../common';
import { ProcessedTx, TxsRange } from '../models';
import { RedisHelperService, HelperService } from '../helper';
import { WorkerClientService } from './worker-client';
import { NotificationService } from './notification-service';

@Injectable()
export class TxsTrackService extends CustomLogger {
  private step = 10;
  private density = 10;

  constructor(
    private pg: PgService,
    private mongo: MongoService,
    private processor: TxProcessorService,
    private redis: RedisHelperService,
    private workerClient: WorkerClientService,
    private notificationService: NotificationService,
    private helper: HelperService,
  ) {
    super('TxsTrackService');
  }

  private async onApplicationBootstrap(): Promise<any> {
    await this.helper.sleep(14000); // wait websocket service to give us the network height

    const trackAt = await this.getTxInitialTrackAt();
    this.setTxPrevTrackAt(trackAt);
    this.trackTxs();
  }

  async trackTxs(): Promise<void> {
    const range = await this.getTxsTrackRange();
    const { $lte } = range;

    this.logger.verbose({
      ...range,
      step: this.step,
      density: this.density,
      prevSize: this.density * this.step,
    });

    const { rawTxs, syncHeight } = await this.mongo.getRawTxsForRange(range);
    let prevTrackAt: number;

    this.density = rawTxs.length / (range.$lte - range.$gt);

    if (rawTxs.length === 0) {
      this.setTxPrevTrackAt(syncHeight);
    }

    const txs: ProcessedTx[] = await this.processor.processTxs(rawTxs);

    this.workerClient.notifyBalancesChangeByTxs(txs);
    this.workerClient.notifyTxDecodingProgress({ block: $lte });

    if (txs.length) {
      const pgSavedResult = await this.pg.saveProcessedTxs(txs);
      const { txsIsSaved, erc20Count, txsCount } = pgSavedResult;
      await this.helper.sleep(2000);
      if (txsIsSaved) {
        prevTrackAt = syncHeight;
        this.notificationService.makeStatsForTxs(txs);
        this.workerClient.notifyTxStats({ erc20: erc20Count, txs: txsCount });
      } else {
        this.logErrorMsg(`Failed to save txs to pg.`);
        this.logErrorMsg(`Will retry this range`);
        this.logErrorMsg(`Retrying...`);
      }
    } else {
      const networkHeight = await this.helper.getNetworkHeight();
      if (networkHeight - range.$lte > 1000) {
        prevTrackAt = range.$lte;
      } else {
        prevTrackAt = range.$gt;
      }
      this.density = 10;
    }

    this.setTxPrevTrackAt(prevTrackAt);

    const networkHeight = await this.helper.getNetworkHeight();
    if (networkHeight === prevTrackAt) await this.helper.sleep(1000);
    this.trackTxs();
  }

  async getTxPrevTrackAt(): Promise<number> {
    const trackAtKey = this.getTrackAtKey();
    const trackAt = await this.redis
      .getCachedValue(trackAtKey)
      .then(val => +val);
    if (trackAt) return +trackAt;
    return this.getTxInitialTrackAt().then(trackAt => +trackAt);
  }

  private setTxPrevTrackAt(height: number): void {
    this.logInfoMsg(`Set track at ${height}`);
    const trackAtKey = this.getTrackAtKey();
    this.redis.cacheValue(trackAtKey, height);
  }

  private async getTxsTrackRange(): Promise<TxsRange> {
    const $gt = await this.getTxPrevTrackAt();
    const networkHeight = await this.helper.getNetworkHeight();
    if (this.density > 100) this.step = 2;
    else if (this.density > 50) this.step = 4;
    else if (this.density > 25) this.step = 8;
    else if (this.density > 9) this.step = 16;
    else if (this.density > 4) this.step = 32;
    else if (this.density > 2) this.step = 64;
    else if (this.density > 1) this.step = 128;
    else if (this.density > 0.08) this.step = 256;
    else this.step = 1000;
    const $lte = Math.min(networkHeight, this.step + +$gt);
    if ($lte === $gt) {
      await this.helper.sleep(12000);
      return this.getTxsTrackRange();
    }
    return { $gt: +$gt, $lte };
  }

  private async getTxInitialTrackAt(): Promise<number> {
    const trackAtKey = this.getTrackAtKey();
    let trackAt: number = await this.pg.getTxTrackStartHeight();
    if (!trackAt) trackAt = await this.mongo.getTxTrackStartHeight();
    if (trackAt) this.redis.cacheValue(trackAtKey, trackAt);
    return trackAt;
  }

  private getTrackAtKey(): string {
    return 'track_at';
  }
}
