import { Injectable } from '@nestjs/common';
import { TxsTrackService } from './txs-track.service';
import { TxsStatsService } from './txs-stats.service';

@Injectable()
export class TaskService {
  constructor(private track: TxsTrackService, private stats: TxsStatsService) {}

  trackTxs() {
    this.track.trackTxs();
  }

  statsTxs() {
    this.stats.trackTxsStats();
  }

  getTxPrevTrackAt() {
    return this.track.getTxPrevTrackAt();
  }

  getTxsStatsPrevTrackAt() {
    return this.stats.getTxsStatsPrevTrackAt();
  }
}
