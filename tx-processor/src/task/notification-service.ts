import { Injectable } from '@nestjs/common';
import { WorkerClientService } from './worker-client';
import { ProcessedTx, AddressStatsMap } from '../models';
import { TRANSACTION_TYPES } from '../common';
import { from } from 'rxjs';

@Injectable()
export class NotificationService {
  constructor(private workerClient: WorkerClientService) {}

  // How to handle the condition that the application crashes?
  makeStatsForTxs(txs: ProcessedTx[]): void {
    this.notifyAddressInfo(txs);
    this.notifyTokenInfo(txs);
  }

  private notifyTokenInfo(txs: ProcessedTx[]): void {
    const tokenMap = {};
    txs.map(({ assets = [], type, hash }) => {
      if (type === TRANSACTION_TYPES['GenAssetFunc'].id) {
        this.workerClient.notifyTokenGeneration({ tx: hash });
      }
      if (type === TRANSACTION_TYPES['AssetValueChangeFunc'].id) {
        this.workerClient.notifyTokenChange({ token: assets[0] });
      }
      assets.map(asset => {
        if (tokenMap[asset]) tokenMap[asset] += 1;
        else tokenMap[asset] = 1;
      });
    });
    from(Object.keys(tokenMap)).subscribe(token => {
      this.workerClient.notifyTokenStats({ token, count: tokenMap[token] });
    });
  }

  private notifyAddressInfo(txs: ProcessedTx[]): void {
    const addressMap: AddressStatsMap = {};
    const erc20Tokens = new Set<string>();
    txs.map(({ sender, receiver, timestamp, assets = [], data = {} }) => {
      assets.map(asset => asset.length === 42 && erc20Tokens.add(asset));
      this.makeStatsForAddress(addressMap, {
        address: sender,
        timestamp,
        usan: data.usan,
      });
      this.makeStatsForAddress(addressMap, { address: receiver, timestamp });
    });

    // add an erc20 tag for an address
    Array.from(erc20Tokens).map(token => {
      if (addressMap[token]) addressMap[token].erc20 = true;
      else addressMap[token] = { address: token, erc20: true };
    });

    from(Object.keys(addressMap)).subscribe(address => {
      this.workerClient.notifyAddressStats({ address, ...addressMap[address] });
    });
  }

  private makeStatsForAddress(
    addressMap: AddressStatsMap,
    stats: {
      address: string;
      timestamp: number;
      usan?: number;
    },
  ): AddressStatsMap {
    const { address, timestamp, usan } = stats;

    if (!address) return addressMap;

    if (addressMap[address]) {
      let { txs, create_at, active_at } = addressMap[address];
      txs += 1;
      create_at = Math.min(create_at, timestamp);
      active_at = Math.min(active_at, timestamp);
      addressMap[address] = { address, create_at, active_at, txs };
    } else {
      const create_at = timestamp;
      const active_at = timestamp;
      addressMap[address] = { address, create_at, active_at, txs: 1 };
    }

    if (usan) addressMap[address].usan = usan;

    return addressMap;
  }
}
