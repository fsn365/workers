import { Injectable } from '@nestjs/common';
import { TokenClientService } from './token-client.service';
import { AddressClientService } from './address-client.service';
import { ServerClientService } from './server-client.service';
import { BalanceClientService } from './balance-client.service';
import {
  ClientMsg,
  ProcessedTx,
  TxRelatedBalance,
  AddressStatsMetadata,
} from '../../models';
import { TRANSACTION_TYPES, FSN_CONTRACT } from '../../common';

@Injectable()
export class WorkerClientService {
  constructor(
    private tokenClient: TokenClientService,
    private addressClient: AddressClientService,
    private serverClient: ServerClientService,
    private balanceClient: BalanceClientService,
  ) {}

  private notify(service: string, msg: ClientMsg<any>): void {
    switch (service) {
      case 'address':
        this.addressClient.notify(msg);
        break;
      case 'token':
        this.tokenClient.notify(msg);
        break;
      case 'server':
        this.serverClient.notify(msg);
        break;
      case 'balance':
        this.balanceClient.notify(msg);
        break;
      default:
        break;
    }
  }

  notifyBalancesChangeByTxs(txs: ProcessedTx[]): void {
    if (!txs.length) return;
    txs.map(({ sender, receiver, assets = [], status, type }) => {
      const msg: TxRelatedBalance = { s: sender };
      if (assets.length) msg.assets = assets;
      if (status && receiver && receiver !== FSN_CONTRACT) msg.r = receiver;

      let pattern: string;
      if (type === TRANSACTION_TYPES['TimeLockFunc'].id) pattern = 'tl_balance';
      else pattern = 'balance';
      this.notify('balance', { pattern, data: msg });
    });
  }

  notifyTxDecodingProgress(msg: { block: number }): void {
    this.notify('server', { pattern: 'tx:progress', data: msg });
  }

  notifyTxStats(msg: { erc20: number; txs: number }): void {
    this.notify('server', { pattern: 'tx:count', data: msg });
  }

  notifyTokenGeneration(msg: { tx: string }): void {
    this.notify('token', { pattern: 'token:new', data: msg });
  }

  notifyTokenChange(msg: { token: string }): void {
    this.notify('token', { pattern: 'token:change', data: msg });
  }

  notifyTokenStats(msg: { token: string; count: number }): void {
    this.notify('token', { pattern: 'token:stats', data: msg });
  }

  notifyAddressStats(msg: AddressStatsMetadata): void {
    this.notify('address', { pattern: 'address', data: msg });
  }

  notifyAddressInfo(msg: {
    address: string;
    exchange?: boolean;
    contract?: boolean;
  }): void {
    this.notify('address', { pattern: 'address', data: msg });
  }
}
