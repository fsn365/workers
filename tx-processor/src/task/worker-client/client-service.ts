import { ClientProxy } from '@nestjs/microservices';
import { CustomLogger } from '../../common';
import { ClientMsg } from '../../models';

export abstract class ClientSerivce<T> extends CustomLogger {
  protected client: ClientProxy;
  protected patterns: string[];
  private name: string;

  constructor(name: string) {
    super(name);
    this.name = name;
  }

  private async onApplicationBootstrap(): Promise<void> {
    this.logInfoMsg(`${this.name} connected...`);
    await this.client.connect().catch(e => {});
  }

  notify(msg: ClientMsg<T>): void {
    const { pattern, data } = msg;
    if (this.patterns.includes(pattern)) {
      const service = this.name;
      this.logInfoMsg(`service:${service}, pattern:${pattern}`);
      this.logger.verbose(data);
      this.client.emit(pattern, data);
    }
  }
}
