import { Injectable } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { ProcessedTx, PgTx, RangeTxsStats } from '../../models';
import { CustomLogger } from '../../common';

@Injectable()
export class PgService extends CustomLogger {
  constructor(@InjectKnex() private knex: Knex) {
    super('PgService');
  }

  getTxTrackStartHeight(): Promise<number> {
    this.logInfoMsg(`Get tx track start height from pg.`);
    return this.knex('txs')
      .select('block', 'id')
      .orderBy('id', 'desc')
      .limit(1)
      .first()
      .then(record => {
        if (record) {
          const startAt = record.block;
          this.logInfoMsg(`Tx track start height from pg:${startAt}.`);
          return startAt;
        }
      });
  }

  getTxStatsTrackStartTime(): Promise<number> {
    this.logInfoMsg(`Get txs stats start time from pg.`);
    return this.knex('txs_stats')
      .select('stats_at')
      .orderBy('stats_at', 'desc')
      .limit(1)
      .first()
      .then((record: { stats_at: number }) => {
        if (record) return record.stats_at;
      });
  }

  async saveTxsStats(statsData: RangeTxsStats): Promise<boolean> {
    const { stats_at, stats } = statsData;
    this.logInfo({
      method: 'saveTxsStats',
      data: statsData,
    });
    return this.knex(`txs_stats`)
      .insert({ stats_at, stats: JSON.stringify(stats) })
      .then(() => {
        this.logInfoMsg(`Saved txs stats`);
        return true;
      })
      .catch(e => {
        this.logError({ method: `saveTxsStats`, e, data: statsData });
        return false;
      });
  }

  async saveProcessedTxs(
    txs: ProcessedTx[],
  ): Promise<{ txsIsSaved: boolean; txsCount?: number; erc20Count?: number }> {
    const startAt = Date.now();
    const size = txs.length;
    this.logInfoMsg(`Ready to insert ${size} txs to pg.`);

    const provider = this.getTrxProvider();
    const trx = await provider();
    const promises: Promise<any>[] = [];

    const erc20Txs = this.getProcessedErc20Txs(txs);
    const allTxs = this.processTxs(txs);

    if (allTxs.length) promises.push(this.saveOverallTxs(allTxs, provider));
    if (erc20Txs.length) promises.push(this.saveErc20Txs(erc20Txs, provider));

    return Promise.all(promises).then(data => {
      const [txs, erc20] = data;
      const failed = new Set(data).has(-1);
      const result: any = { txsIsSaved: !failed };

      if (failed) trx.rollback();
      else {
        if (erc20) result.erc20Count = erc20;
        if (txs) result.txsCount = txs;
        trx.commit();
      }

      const cost = Date.now() - startAt;
      this.logInfoMsg(`Saved ${size} txs to pg, cost ${cost} ms.`);

      return result;
    });
  }

  private getTrxProvider(): any {
    return this.knex.transactionProvider();
  }

  private async saveErc20Txs(txs: PgTx[], provider: any): Promise<number> {
    if (!txs.length) return 0;
    const trx = await provider();
    return trx
      .returning('id')
      .insert(txs)
      .into('erc20_txs')
      .then((ids: number[]) => Math.max(...ids))
      .catch((e: any) => {
        this.logError({ method: 'insertErc20Txs', e });
        return -1;
      });
  }

  private async saveOverallTxs(txs: PgTx[], provider: any): Promise<number> {
    const trx = await provider();
    return trx
      .returning('id')
      .insert(txs)
      .into('txs')
      .then((ids: number[]) => Math.max(...ids))
      .catch((e: any) => {
        this.logError({ method: 'saveOverallTxs', e });
        return -1;
      });
  }

  private processTxs(txs: ProcessedTx[]): PgTx[] {
    return txs.map(tx => {
      const { data, assets, ...rest } = tx;
      const pgTx: PgTx = { ...rest };
      if (data) pgTx.data = JSON.stringify(data);
      if (assets) pgTx.assets = JSON.stringify(assets);
      return pgTx;
    });
  }

  private getProcessedErc20Txs(txs: ProcessedTx[]): PgTx[] {
    const erc20Txs = txs.filter(tx => {
      const { assets = [] } = tx;
      let isErc20Tx = false;
      assets.map(asset => {
        if (asset.length === 42) isErc20Tx = true;
      });
      if (isErc20Tx) return tx;
    });
    return this.processTxs(erc20Txs);
  }
}
