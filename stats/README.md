# Fusion Stats Services

## Functionality

## Message supported

Please refer [app.controller.ts](./src/app.controller.ts);


## How to start service

- install dependencies

```bash
npm install
```

- build file

```bash
npm run build
```

- start service

```bash
pm2 start ecosystem
```