import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { MicroserviceOptions } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = app.get(ConfigService);

  // Hybrid app to get websocket data
  app.connectMicroservice<MicroserviceOptions>(config.get('service:app'));
  app.connectMicroservice<MicroserviceOptions>(config.get('service:web3'));
  await app.startAllMicroservicesAsync();
  await app.listen(9004);

  const logger = new Logger('worker:stats');
  logger.log(`worker:stats started at url:${await app.getUrl()}\n`);
}
bootstrap();
