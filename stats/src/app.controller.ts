import { Controller, Logger } from '@nestjs/common';
import {
  Payload,
  MessagePattern,
  Ctx,
  RmqContext,
} from '@nestjs/microservices';
import { AppService } from './app.service';
import { PgAddress } from './models';

@Controller()
export class AppController {
  private logger = new Logger(`AssetWorker:AppController`);

  constructor(private readonly service: AppService) {}

  @MessagePattern('network:block')
  updateNetworkHeight(@Payload() msg: any) {
    this.service.updateNetworkStat(msg);
  }

  @MessagePattern('address')
  statsAddress(@Payload() msg: Partial<PgAddress>, @Ctx() ctx: RmqContext) {
    this.service.makeAddressStats(msg).then(success => {
      if (success) this.ackMsg(ctx);
    });
  }

  private ackMsg(ctx: RmqContext) {
    const rawMsg = ctx.getMessage();
    const channel = ctx.getChannelRef();
    channel.ack(rawMsg);

    const msg = JSON.parse(rawMsg.content.toString());
    this.logger.log(`Ack ${msg.pattern}, data:`);
    this.logger.verbose(msg.data);
  }
}
