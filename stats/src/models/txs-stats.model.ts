export interface TxsStats {
  stats_at: number;
  stats: {
    all: number;
    ne_tickets: number;
  };
}
