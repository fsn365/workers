export interface MinersStats {
  hash: string;
  blocks: number;
  reward: number;
  active_at: number;
  create_at: number;
}
