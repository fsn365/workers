import { Transport } from '@nestjs/microservices';
import { Web3Server } from './services';

export const config = () => ({
  postgres: {
    client: 'pg',
    connection: {
      host: process.env.db_host,
      port: process.env.db_port,
      user: process.env.db_username,
      password: process.env.db_password,
      database: process.env.db_name,
    },
    searchPath: ['public'],
    pool: {
      min: 0,
      max: 2,
    },
  },

  mongodb: {
    uri: process.env['mongo_uri'],
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },

  redis: {
    name: 'stats',
    host: process.env.redis_host || 'localhost',
    port: process.env.redis_port || 6379,
  },

  ['service:web3']: {
    strategy: new Web3Server(process.env.wss_url),
  },
  ['worker:balance']: {
    transport: Transport.RMQ,
    options: {
      urls: [process.env.address_rabbitmq_url],
      queue: process.env.address_rabbitmq_queue,
      prefetchCount: 1,
      noAck: false,
      queueOptions: {
        durable: true,
      },
    },
  },

  rpc: process.env.rpc_url,

  app: {
    name: 'Worker:Stats',
    transport: Transport.RMQ,
    options: {
      urls: [process.env.asset_rabbitmq_url],
      queue: process.env.asset_rabbitmq_queue,
      prefetchCount: 1,
      noAck: false,
      queueOptions: { durable: true },
    },
  },
});
