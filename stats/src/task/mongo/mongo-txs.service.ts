import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import TxDoc from './transaction.interface';
import { CustomLogger } from '../../common';
import { TxsStats } from '../../models';

@Injectable()
export class MongoTxsService extends CustomLogger {
  constructor(@InjectModel('Transactions') private model: Model<TxDoc>) {
    super('MongoTxsService');
  }

  getTxsStats(anchor: number, range: number): Promise<TxsStats> {
    const $lte = range + anchor;
    const filter = { timestamp: { $lte, $gt: anchor } };
    const statsAll = this.statsAllTxs(filter);
    const statsTickets = this.statsTicketTxs(filter);
    return Promise.all([statsAll, statsTickets])
      .then(data => {
        const [all, tickets] = data;
        if (new Set(data).has(-1)) return null;
        return { stats: { all, ne_tickets: all - tickets }, stats_at: $lte };
      })
      .catch(e => {
        this.logError({ method: 'getTxsStats', e });
        return null;
      });
  }

  getLatestTxTimeInMongo(): Promise<number> {
    return this.model
      .find({}, { timestamp: 1, _id: 0 })
      .sort({ timestamp: -1 })
      .limit(1)
      .then(docs => docs[0].toJSON())
      .then(doc => doc.timestamp);
  }

  getStartTime(): Promise<number> {
    return this.model
      .find({}, { _id: 0, timestamp: 1 })
      .sort({ timestamp: 1 })
      .limit(1)
      .then(docs => {
        const doc = docs[0].toJSON();
        return doc.timestamp;
      });
  }

  private statsAllTxs(filter: {
    timestamp: { $lte: number; $gt: number };
  }): Promise<number> {
    return this.model
      .aggregate([
        { $match: filter },
        { $project: { _id: 1 } },
        { $count: 'total' },
      ])
      .then(records => records[0].total)
      .catch(e => {
        this.logError({ method: 'statsAllTxs', e });
        return -1;
      });
  }

  private statsTicketTxs(filter: {
    timestamp: { $lte: number; $gt: number };
  }): Promise<number> {
    const $match = { type: 'BuyTicketFunc', ...filter };
    return this.model
      .aggregate([{ $match }, { $project: { _id: 1 } }, { $count: 'total' }])
      .then(records => records[0].total)
      .catch(e => {
        this.logError({ method: 'statsTicketTxs', e });
        return -1;
      });
  }
}
