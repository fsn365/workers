import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CustomLogger } from '../../common';
import BlockDoc from './block.interface';
import { MinerAddress } from '../../models';

@Injectable()
export class MongoBlocksService extends CustomLogger {
  constructor(@InjectModel('Transactions') private model: Model<BlockDoc>) {
    super('MongoBlocksService');
  }

  makeMinersStats(
    anchor: number,
  ): Promise<{ syncHeight: number; miners?: MinerAddress[] }> {
    const $match: any = { number: { $gt: anchor } };
    let syncHeight = anchor;
    return this.model
      .aggregate([
        { $match },
        { $group: { _id: '$miner', lBk: { $max: '$number' } } },
      ])
      .then(stats => {
        if (!stats.length) return { syncHeight };
        const miners: MinerAddress[] = stats.map(statsItem => {
          const { _id, lBk } = statsItem;
          syncHeight = Math.max(syncHeight, lBk);
          return { hash: _id as string, miner: true };
        });
        return { syncHeight, miners };
      })
      .catch(e => {
        this.logError({ method: 'getMinersStats', e });
        return { syncHeight };
      });
  }
}
