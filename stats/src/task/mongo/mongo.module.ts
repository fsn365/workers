import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { MongoConfig } from './mongo.config';
import { MongoTxsService } from './mongo-txs.service';
import { MongoBlocksService } from './mongo-blocks.service';
import transactionSchema from './transaction.schema';
import blockSchema from './block.schema';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useClass: MongoConfig,
    }),
    MongooseModule.forFeature([
      { name: 'Transactions', schema: transactionSchema },
      { name: 'Blocks', schema: blockSchema },
    ]),
  ],
  providers: [MongoTxsService, MongoBlocksService],
  exports: [MongoTxsService, MongoBlocksService],
})
export class MongoModule {}
