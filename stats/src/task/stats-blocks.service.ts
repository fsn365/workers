import { Injectable } from '@nestjs/common';
import { MongoBlocksService } from './mongo';
import { RedisHelperService, HelperService } from './helper';
import { PgAddressService } from './pg';
import { CustomLogger } from '../common';
import { BalanceService } from './balance/balance.service';

@Injectable()
export class StatsBlocksService extends CustomLogger {
  constructor(
    private mongo: MongoBlocksService,
    private pg: PgAddressService,
    private redis: RedisHelperService,
    private helper: HelperService,
    private balance: BalanceService,
  ) {
    super('StatsBlocksService');
  }

  async makeMinersStats() {
    const prevTrackAt = await this.getPrevTrackAt();
    if (!prevTrackAt) this.setHaltedTime();

    const stats = await this.mongo.makeMinersStats(prevTrackAt);
    const { miners, syncHeight } = stats;
    if (!miners) return;

    const provider = this.pg.getTransactionProvider();
    const trx = await provider();
    const promises = miners.map(async miner => {
      this.balance.updateAddressBalance(miner.hash);
      const isCahced = await this.isCachedMinerAddress(miner.hash);
      if (isCahced) return true;
      return this.pg.trackAddress(miner, provider).then(result => {
        this.cacheMinersAddress(miner.hash);
        return result;
      });
    });

    await Promise.all(promises)
      .then(data => !new Set(data).has(false))
      .then(async success => {
        if (success) {
          this.setPrevTrackAt(syncHeight);
          trx.commit();
          return;
        }
        const msg = `track miners failed.`;
        throw new Error(msg);
      })
      .catch(async e => {
        this.logError({ method: 'trackMinersAddress', e });
        trx.rollback();
        await this.helper.sleep(1000);
        this.makeMinersStats();
      });
  }

  checkHaltedState(): Promise<boolean> {
    return this.redis.getCachedValue(`stats:block:halt`).then(val => !!val);
  }

  private getPrevTrackAt(): Promise<number> {
    return this.redis.getCachedValue('stats:block').then(val => +val);
  }

  private setPrevTrackAt(height: number): void {
    this.redis.cacheValue('stats:block', JSON.stringify(height));
  }

  // before track miner's information from new block, we wait for 10 minutes
  private setHaltedTime(expireNow?: boolean) {
    this.redis.cacheValue('block:halt', 'yes', 10 * 60);
  }

  private cacheMinersAddress(hash: string) {
    this.redis.cacheValue(`miner:${hash}`, 'miner', HelperService.oneMonth);
  }

  private isCachedMinerAddress(hash: string): Promise<boolean> {
    return this.redis.getCachedValue(`miner:${hash}`).then(val => !!val);
  }
}
