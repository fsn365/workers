import { Module } from '@nestjs/common';
import { MongoModule } from './mongo';
import { PgModule } from './pg';
import { HelperModule } from './helper';
import { BalanceModule, BalanceService } from './balance';
import { StatsTxsService } from './stats-txs.service';
import { StatsBlocksService } from './stats-blocks.service';
import { StatsAddressService } from './stats-address.service';

@Module({
  imports: [MongoModule, PgModule, HelperModule, BalanceModule],
  providers: [
    StatsTxsService,
    StatsBlocksService,
    StatsAddressService,
    BalanceService,
  ],
  exports: [StatsTxsService, StatsBlocksService, StatsAddressService],
})
export class TaskModule {}
