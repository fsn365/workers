import { Injectable } from '@nestjs/common';
import { isEmpty } from 'lodash';
import { CustomLogger } from '../common';
import { RedisHelperService, HelperService } from './helper';
import { PgAddressService } from './pg';
import { PgAddress } from '../models';

@Injectable()
export class StatsAddressService extends CustomLogger {
  constructor(private pg: PgAddressService, private redis: RedisHelperService) {
    super(`StatsAddressService`);
  }

  async makeAddressStats(addressData: Partial<PgAddress>): Promise<boolean> {
    const isCached = await this.checkTagedAddress(addressData);
    if (isCached) return true;

    const provider = this.pg.getTransactionProvider();
    const trx = await provider();
    return this.pg
      .trackAddress(addressData, provider)
      .then(succuess => {
        if (succuess) {
          trx.commit();
          this.cacheTypedAddress(addressData);
        } else trx.rollback();
        return succuess;
      })
      .catch(e => {
        this.logError({ method: 'makeAddressStats', e });
        trx.rollback();
        return false;
      });
  }

  cacheTypedAddress(addressData: Partial<PgAddress>) {
    const { erc20, exchange, hash } = addressData;
    const isAddressTypeUpdate = this.isAddressTypeUpdate(addressData);

    let key: string;
    if (erc20 && isAddressTypeUpdate) {
      key = `erc20:${hash}`;
      this.redis.cacheValue(key, 'erc20', HelperService.oneMonth);
    }
    if (exchange && isAddressTypeUpdate) {
      this.redis.cacheValue(key, 'miner', HelperService.oneMonth);
    }
  }

  private async checkTagedAddress(addressData: Partial<PgAddress>) {
    const { erc20, exchange, hash } = addressData;
    if (erc20) {
      return this.redis.getCachedValue(`erc20:${hash}`).then(val => !!val);
    }
    if (exchange) {
      return this.redis.getCachedValue(`exchange:${hash}`).then(val => !!val);
    }
  }

  private isAddressTypeUpdate(addressData: Partial<PgAddress>): boolean {
    const { exchange, erc20, hash, ...others } = addressData;
    if (isEmpty(others)) return true;
  }
}
