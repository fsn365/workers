import { Module, Global } from '@nestjs/common';
import { RedisHelperModule, RedisHelperService } from './redis-helper';
import { HelperService } from './helper.service';

@Global()
@Module({
  imports: [HelperModule, RedisHelperModule],
  providers: [HelperService, RedisHelperService],
  exports: [HelperService, RedisHelperService],
})
export class HelperModule {}
