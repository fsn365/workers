import { Injectable } from '@nestjs/common';
import { RedisHelperService } from './redis-helper';

@Injectable()
export class HelperService {
  static oneDay = 86400;
  static oneMonth = HelperService.oneDay * 30;

  constructor(private redis: RedisHelperService) {}

  storeNetworkStat(state: { time: number; height: number }): void {
    this.storeNetworkTime(state.time);
    this.storeNetworkHeight(state.height);
  }

  getNetworkHeight(): Promise<number> {
    return this.redis.getCachedValue('network:height').then(val => +val);
  }

  getNetworkTime(): Promise<number> {
    return this.redis.getCachedValue('network:time').then(time => +time);
  }

  sleep(ms: number) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve();
      }, ms);
    });
  }

  private storeNetworkTime(time: number) {
    this.redis.cacheValue('network:time', JSON.stringify(time));
  }

  private storeNetworkHeight(height: number): void {
    return this.redis.cacheValue('network:height', JSON.stringify(height));
  }
}
