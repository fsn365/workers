import { Injectable } from '@nestjs/common';
import { CustomLogger } from '../common';
import { MongoTxsService } from './mongo';
import { PgService } from './pg';
import { RedisHelperService, HelperService } from './helper';

@Injectable()
export class StatsTxsService extends CustomLogger {
  constructor(
    private mongo: MongoTxsService,
    private pg: PgService,
    private redis: RedisHelperService,
  ) {
    super('StatsTxsService');
  }

  async onApplicationBootstrap(): Promise<any> {
    let prevTrackAt = await this.pg.getPrevTrackAt();
    if (!prevTrackAt) prevTrackAt = await this.mongo.getStartTime();
    this.setPrevTrackAt(prevTrackAt);
    this.makeTxsStats(HelperService.oneDay);
  }

  async makeTxsStats(rate?: number): Promise<void> {
    this.logInfo({ method: 'makeTxsStats', data: { rate } });

    const trackAt = await this.getPrevTrackAt();
    const $lte = trackAt + rate;
    const latestTxTimeInMongo = await this.mongo.getLatestTxTimeInMongo();
    if ($lte > latestTxTimeInMongo) return;
    const stats = await this.mongo.getTxsStats(trackAt, rate);
    await this.pg.createTxsStats(stats).then(result => {
      if (result) {
        this.setPrevTrackAt(stats.stats_at);
        this.makeTxsStats(rate);
      }
    });
  }

  async getPrevTrackAt(): Promise<number> {
    let trackAt = await this.redis
      .getCachedValue('txs:track_at')
      .then(val => +val)
      .catch(() => 0);
    if (trackAt) return trackAt;

    trackAt = await this.pg.getPrevTrackAt();
    if (trackAt) return trackAt;

    return this.mongo.getStartTime();
  }

  private setPrevTrackAt(timestamp: number) {
    this.redis.cacheValue('txs:track_at', JSON.stringify(timestamp));
  }
}
