import { Injectable, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { SERVICE_NAME } from './balance.client.provider';
import { FSN_TOKEN } from '../../constant';
import { CustomLogger } from '../../common';

@Injectable()
export class BalanceService extends CustomLogger {
  constructor(@Inject(SERVICE_NAME) private readonly client: ClientProxy) {
    super('BalanceService');
  }

  async onApplicationBootstrap(): Promise<any> {
    await this.client.connect();
    this.logMsg(`\nBalance client connected....\n`);
  }

  async updateAddressBalance(address: string): Promise<void> {
    this.client.emit('address:balance', { s: address, assets: [FSN_TOKEN] });
  }
}
