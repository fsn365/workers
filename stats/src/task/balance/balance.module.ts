import { Module } from '@nestjs/common';
import { BalanceService } from './balance.service';
import { BALANCE_CLIENT_PROVIDER } from './balance.client.provider';

@Module({
  providers: [BALANCE_CLIENT_PROVIDER, BalanceService],
  exports: [BalanceService],
})
export class BalanceModule {}
