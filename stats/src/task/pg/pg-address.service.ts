import { Injectable } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { CustomLogger } from '../../common';
import { PgAddress } from '../../models';

@Injectable()
export class PgAddressService extends CustomLogger {
  constructor(@InjectKnex() private knex: Knex) {
    super('PgAddressService');
  }

  getTransactionProvider() {
    return this.knex.transactionProvider();
  }

  async trackAddress(data: Partial<PgAddress>, provider: any) {
    const trx = await provider();
    const { hash } = data;
    const isExist = await trx
      .where({ hash })
      .select('create_at', 'txs', 'active_at')
      .from('address')
      .limit(1)
      .first()
      .catch(e => null);

    if (!isExist) return this.createAddress(data, provider);

    const { create_at, active_at, txs, ...others } = data;
    const update: Partial<PgAddress> = others;
    const oldCreateAt = isExist.create_at || Infinity;
    const oldActiveAt = isExist.active_at || -Infinity;
    const oldTxs = isExist.txs || 0;
    if (create_at) update.create_at = Math.min(create_at, oldCreateAt);
    if (active_at) update.active_at = Math.max(active_at, oldActiveAt);
    if (txs) update.txs = txs + oldTxs;
    if (Object.keys(update).length === 0) return true;
    return this.updateAddress(update, provider);
  }

  private async createAddress(
    data: Partial<PgAddress>,
    provider: any,
  ): Promise<boolean> {
    this.logInfo({ method: 'createAddress', data });
    const trx = await provider();
    return trx
      .insert(data)
      .into('address')
      .then(() => true)
      .catch(e => {
        this.logError({ method: 'createAddress', e, data });
        return false;
      });
  }

  private async updateAddress(
    data: Partial<PgAddress>,
    provider: any,
  ): Promise<boolean> {
    this.logInfo({ method: 'updateAddress', data });
    const { hash, ...update } = data;
    const trx = await provider();
    return trx
      .update(update)
      .from('address')
      .where({ hash })
      .then(() => true)
      .catch(e => {
        this.logError({ method: 'updateAddress', e, data });
        return false;
      });
  }
}
