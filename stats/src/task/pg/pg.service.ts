import { Injectable } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { CustomLogger } from '../../common';

@Injectable()
export class PgService extends CustomLogger {
  constructor(@InjectKnex() private readonly knex: Knex) {
    super('worker:stats:PgService');
  }

  createTxsStats(info: { stats_at: number; stats: any }): Promise<boolean> {
    const { stats_at, stats } = info;
    const record = { stats_at, stats: JSON.stringify(stats) };
    this.logInfo({ method: 'createTxsStats', data: info });
    return this.knex.transaction(trx => {
      return trx
        .insert(record)
        .into('txs_stats')
        .then(() => true)
        .catch(e => {
          this.logError({ method: 'createTxsStats', e });
          return false;
        });
    });
  }

  getPrevTrackAt(): Promise<number> {
    return this.knex('txs_stats')
      .select('stats_at')
      .orderBy('stats_at', 'desc')
      .limit(1)
      .first()
      .then(record => {
        if (record) return record.stats_at;
        return null;
      })
      .catch(e => {
        this.logError({ method: 'getStartTime', e });
        return null;
      });
  }
}
