import { Injectable } from '@nestjs/common';
import {
  StatsTxsService,
  StatsBlocksService,
  StatsAddressService,
} from './task';
import { HelperService } from './task/helper';
import { PgAddress } from './models';
import { CustomLogger } from './common';

@Injectable()
export class AppService extends CustomLogger {
  constructor(
    private txs: StatsTxsService,
    private helper: HelperService,
    private blocks: StatsBlocksService,
    private address: StatsAddressService,
  ) {
    super('AppService');
  }

  async updateNetworkStat(blockData: any) {
    const time = blockData.timestamp;
    const height = blockData.number;
    this.helper.storeNetworkStat({ time, height });

    await this.helper.sleep(100);

    this.makeNetworkStats();
  }

  async makeAddressStats(addressData: Partial<PgAddress>): Promise<boolean> {
    const waitForInitMiners = await this.waitTrackMinersInBatch();
    if (waitForInitMiners) await this.helper.sleep(20 * 60);
    return this.address.makeAddressStats(addressData);
  }

  async makeNetworkStats() {
    this.makeTxsStats();
    this.makeMinersStats();
  }

  private async makeTxsStats() {
    const networkTime = await this.helper.getNetworkTime();
    const prevTrackAt = await this.txs.getPrevTrackAt();
    if (prevTrackAt && networkTime - prevTrackAt < HelperService.oneDay) return;
    this.txs.makeTxsStats(HelperService.oneDay);
  }

  private async makeMinersStats() {
    const waitForInit = await this.waitTrackMinersInBatch();
    if (waitForInit) return;
    this.blocks.makeMinersStats();
  }

  private waitTrackMinersInBatch(): Promise<boolean> {
    return this.blocks.checkHaltedState();
  }
}
