const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '.env') });

module.exports = {
  apps : [{
    name: 'asset worker',
    script: 'dist/main.js',
    instances: 2,
    autorestart: true,
    watch: false,
    max_memory_restart: '256M',
  }],
};
