# Fusion Asset Services

## Functionality

- track asset(none smart contract)  generation on fusion blockchain
- track asset(none smart contract) issue amount changes
- store asset key information - *symbol*, *precision* for transaction decode usage.

## Message supported

Please refer [app.controller.ts](./src/app.controller.ts);


## How to start service

- install dependencies

```bash
npm install
```

- build file

```bash
npm run build
```

- start service

```bash
pm2 start ecosystem
```