# tables in PostgreSQL

- assets table

```sql
create table assets (
  id          serial      primary key,
  hash        char(66)    unique not null,
  name        text        not null,
  symbol      text        not null,
  --- some assets' qty is out of range
  qty         text        not null,
  --- some assets' decimals is out of range
  precision   text        not null,
  issuer      char(42)    not null,
  issue_time  int         not null,
  issue_tx    char(66)    not null,
  platform    int         default 0,
  canchange   boolean     not null,
  verified    boolean     default false,
  holders     int         default 1,
  description json        default null
);
```