import { Injectable, Logger } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { TokenStats, TokenData } from '../models';

@Injectable()
export class PgService {
  private readonly logger = new Logger('AssetWorker:PgService');

  constructor(@InjectKnex() private readonly knex: Knex) {}

  getTrxProvider(): any {
    return this.knex.transactionProvider();
  }

  async checkExistence(asset: string, provider: any): Promise<TokenStats> {
    const table = asset.length === 42 ? 'erc20' : 'assets';
    const trx = await provider();
    return trx
      .where({ hash: asset })
      .select('holders', 'txs')
      .from(table)
      .limit(1)
      .first()
      .catch(e => {
        this.error({ method: 'checkExist', data: asset, e });
        process.exit();
      });
  }

  async createToken(token: TokenData, provider: any): Promise<boolean> {
    const table = token.hash.length === 42 ? 'erc20' : 'assets';
    this.log({ method: 'createToken', data: token });
    const trx = await provider();
    return trx
      .insert(token)
      .into(table)
      .then(() => {
        trx.commit();
        return true;
      })
      .catch(e => {
        trx.rollback();
        this.error({ method: 'createToken', data: token, e });
        return false;
      });
  }

  async updateToken(
    { asset, update }: { asset: string; update: any },
    provider: any,
  ): Promise<boolean> {
    const trx = await provider();
    const isErc20 = asset.length === 42;
    const tokenTable = isErc20 ? 'erc20' : 'assets';

    return trx
      .update(update)
      .from(tokenTable)
      .where({ hash: asset })
      .then(() => {
        trx.commit();
        return true;
      })
      .catch((e: any) => {
        this.error({ method: 'udpateToken', e, data: { asset, update } });
        trx.rollback();
        return false;
      });
  }

  private error({ method, data, e }: any): void {
    this.logger.error(method);
    this.logger.verbose(e);
    this.logger.verbose(data);
  }

  private log({ method, data }): void {
    this.logger.log(method);
    this.logger.verbose(data);
  }
}
