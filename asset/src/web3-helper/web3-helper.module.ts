import { Module } from '@nestjs/common';
import { Web3HelperService } from './web3-helper.service';
import { RedisHelperModule } from '../redis-helper/redis-helper.module';

@Module({
  imports: [RedisHelperModule],
  providers: [Web3HelperService],
  exports: [Web3HelperService],
})
export class Web3HelperModule {}
