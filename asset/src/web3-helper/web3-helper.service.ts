import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { abi } from './abi';

const Web3 = require('web3');
const web3FusionExtend = require('web3-fusion-extend');

@Injectable()
export class Web3HelperService {
  private logger = new Logger(`AssetWorker:Web3`);
  private web3: any;

  constructor(config: ConfigService) {
    const RPC_PROVIDER = config.get('rpc_url');
    this.web3 = web3FusionExtend.extend(new Web3(RPC_PROVIDER));
  }

  async getTokenInfo(address: string): Promise<any> {
    const contract = this.getContract(address);
    const props = ['symbol', 'name', 'decimals', 'totalSupply'];
    const promises = props.map(prop => this.getErc20Prop(contract, prop));

    return Promise.all(promises)
      .then(data => {
        let [symbol, name, precision, qty] = data;
        qty = +qty;
        precision = +precision;
        qty = qty / Math.pow(10, precision);
        const token = { symbol, name, precision, qty, hash: address };
        return token;
      })
      .catch(e => {
        this.logger.error(`Get contract ${address} info failed.`);
        this.logger.log(JSON.stringify(e));
        return null;
      });
  }

  async getTokenSupply(address: string): Promise<number> {
    const contract = this.getContract(address);
    const props = ['decimals', 'totalSupply'];

    const promises = props.map((prop: string) =>
      this.getErc20Prop(contract, prop),
    );
    return Promise.all(promises)
      .then(data => {
        const [precision, supply] = data;
        return +supply / Math.pow(10, precision);
      })
      .catch(() => -1);
  }

  private getContract(address: string) {
    return new this.web3.eth.Contract(abi, address);
  }

  private getErc20Prop(contract: any, prop: string): Promise<any> {
    return new Promise((resolve, reject) => {
      contract.methods[prop]().call((err, res) => {
        if (err) {
          reject(err);
        }
        if (res) {
          resolve(res);
        }
      });
    });
  }
}
