import { Controller, Logger } from '@nestjs/common';
import {
  Ctx,
  Payload,
  RmqContext,
  MessagePattern,
} from '@nestjs/microservices';
import { AppService } from './app.service';
import { TxsCountMsg } from './models';

@Controller()
export class AppController {
  private logger = new Logger(`AssetWorker:AppController`);

  constructor(private readonly service: AppService) {}

  // cmd to stats transaction count, from tx worker
  @MessagePattern('txs')
  updateTxsCount(@Payload() msg: TxsCountMsg, @Ctx() ctx: RmqContext): void {
    this.service.updateTxsCount(msg).then(res => {
      if (res) this.ackMsg(ctx);
    });
  }

  // cmd to stats holders count, from address worker
  @MessagePattern('holders')
  updateHoldersCount(
    @Payload() msg: { asset: string; count: number },
    @Ctx() ctx: RmqContext,
  ): void {
    this.service.updateHoldersCount(msg).then(res => {
      if (res) this.ackMsg(ctx);
    });
  }

  // cmd to track erc20 token, from address worker
  @MessagePattern('erc20')
  trackErc20Token(
    @Payload() msg: { contract: string },
    @Ctx() ctx: RmqContext,
  ): void {
    this.service.trackToken(msg.contract).then(res => {
      if (res) this.ackMsg(ctx);
    });
  }

  // cmd to track fusion token, from tx worker
  @MessagePattern('asset:new')
  createAsset(@Payload() msg: { tx: string }, @Ctx() ctx: RmqContext): void {
    this.service.createToken(msg.tx).then(res => {
      if (res) this.ackMsg(ctx);
    });
  }

  // cmd to track fusion token change, from tx worker
  @MessagePattern('asset:change')
  updateQuantity(
    @Payload() msg: { asset: string },
    @Ctx() ctx: RmqContext,
  ): void {
    this.service.updateTokenSupply(msg.asset).then(res => {
      if (res) this.ackMsg(ctx);
    });
  }

  private ackMsg(ctx: RmqContext) {
    const rawMsg = ctx.getMessage();
    const channel = ctx.getChannelRef();
    channel.ack(rawMsg);

    const msg = JSON.parse(rawMsg.content.toString());
    this.logger.log(`Ack ${msg.pattern}, data:`);
    this.logger.verbose(msg);
  }
}
