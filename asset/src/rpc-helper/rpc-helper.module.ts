import { Module, HttpModule } from '@nestjs/common';
import { RpcHelperService } from './rpc-helper.service';

@Module({
  imports: [HttpModule.register({})],
  providers: [RpcHelperService],
  exports: [RpcHelperService],
})
export class RpcHelperModule {}
