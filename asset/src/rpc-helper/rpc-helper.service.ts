import { Injectable, HttpService, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class RpcHelperService {
  private logger = new Logger('AssetWorker:HelperService');
  constructor(
    private readonly http: HttpService,
    private readonly config: ConfigService,
  ) {}

  async getTokenInfo(asset: string): Promise<any> {
    // https://github.com/fsn-dev/fsn-rpc-api/blob/master/fsn-rpc-api.md#fsn_getasset
    const method = 'fsn_getAsset';
    const RPC_URL = this.config.get('rpc_url');
    return this.http
      .post(
        RPC_URL,
        {
          jsonrpc: '2.0',
          id: 1,
          method,
          params: [asset, 'latest'],
        },
        { headers: { 'Content-Type': 'application/json' } },
      )
      .toPromise()
      .then(res => res.data)
      .then(data => data.result)
      .then(data => {
        const { Total, Symbol, Name, Owner, Decimals } = data;
        const qty = +Total / Math.pow(10, Decimals);
        return {
          qty,
          name: Name,
          issuer: Owner,
          precision: Decimals,
          symbol: Symbol,
        };
      })
      .catch(e => {
        this.logger.log(`Fetch asset:${asset} information failed.`);
        this.logger.log(e);
        return {};
      });
  }
}
