export class TxsCountMsg {
  asset: string;
  txs: number;
}

// tell an address worker that an address is not an erc20 contract address
export interface AddressInfoMsg {
  erc20: boolean;
  hash: string;
}
