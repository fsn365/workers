export interface Erc20TokenInfo {
  hash: string;
  name: string;
  symbol: string;
  qty: number;
  precision: number;
}

export interface TokenInfo extends Erc20TokenInfo {
  issuer: string;
  issue_time: number;
  issue_tx: string;
  canchange: boolean;
  description?: string;
}

export interface TokenStats {
  txs: number;
  holders: number;
}

export type TokenData = Erc20TokenInfo | TokenInfo;
