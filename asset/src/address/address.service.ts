import { Injectable, Inject, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { SERVICE_NAME } from './address.client.provider';
import { AddressInfoMsg } from '../models';

@Injectable()
export class AddressService {
  private logger = new Logger('AssetWorker:AddressClient');

  constructor(@Inject(SERVICE_NAME) private readonly client: ClientProxy) {}

  async onApplicationBootstrap(): Promise<any> {
    await this.client.connect();
    this.logger.log(`Address client connected....\n`);
  }

  async updateAddressInfo(msg: AddressInfoMsg): Promise<void> {
    this.client.emit('address', msg);
  }
}
