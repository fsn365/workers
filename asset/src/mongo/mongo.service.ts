import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import TxDoc from './transaction.interface';

@Injectable()
export class MongoService {
  constructor(
    @InjectModel('Transactions') private readonly model: Model<TxDoc>,
  ) {}

  getTokenInfo(
    txHash: string,
  ): Promise<{ issue_tx: string; issue_time: number; hash: string }> {
    return this.model
      .findById(txHash, {
        _id: 0,
        timestamp: 1,
        log: 1,
      })
      .then(doc => {
        if (!doc) return null;
        const { timestamp, log } = doc.toJSON();
        return { issue_tx: txHash, issue_time: timestamp, hash: log.AssetID };
      })
      .catch(() => null);
  }

  getTokenInfoByID(asset: string) {
    return this.model
      .aggregate([
        { $match: { type: 'GenAssetFunc', 'log.AssetID': asset } },
        {
          $project: {
            _id: 0,
            issue_tx: '$hash',
            issuer: '$from',
            issue_time: '$timestamp',
            log: 1,
          },
        },
        { $limit: 1 },
      ])
      .then(docs => {
        if (!docs.length) return null;

        const { log, ...tx } = docs[0];
        const {
          Symbol,
          Name,
          Decimals,
          canChange,
          Total,
          AssetID,
          Description,
        } = log;
        const qty = Total / Math.pow(10, Decimals);

        let description = null;

        try {
          let desc = JSON.parse(Description);
          if (Object.keys(description).length) description = desc;
        } catch {
          description = { desc: Description };
        }

        return {
          ...tx,
          qty,
          symbol: Symbol,
          name: Name,
          precision: Decimals,
          canchange: canChange,
          hash: AssetID,
          description: JSON.stringify(description),
        };
      })
      .catch(e => {
        console.log(e);
        return null;
      });
  }
}
