import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { config } from './config';
import { AppController } from './app.controller';
import { RedisHelperModule } from './redis-helper/redis-helper.module';
import { RpcHelperModule } from './rpc-helper/rpc-helper.module';
import { Web3HelperModule } from './web3-helper/web3-helper.module';
import { PgModule } from './pg/pg.module';
import { MongoModule } from './mongo/mongo.module';
import { AddressModule } from './address/address.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
    }),
    PgModule,
    MongoModule,
    RedisHelperModule,
    RpcHelperModule,
    Web3HelperModule,
    AddressModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [AppService],
})
export class AppModule {}
