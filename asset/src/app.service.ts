import { Injectable, Logger } from '@nestjs/common';
import { PgService } from './pg/pg.service';
import { TxsCountMsg, TokenInfo } from './models';
import { RpcHelperService } from './rpc-helper/rpc-helper.service';
import { Web3HelperService } from './web3-helper/web3-helper.service';
import { MongoService } from './mongo/mongo.service';
import { RedisHelperService } from './redis-helper/redis-helper.service';
import { AddressService } from './address/address.service';

@Injectable()
export class AppService {
  private logger = new Logger(`AssetWorker:AppService`);

  constructor(
    private pg: PgService,
    private rpc: RpcHelperService,
    private web3: Web3HelperService,
    private mongo: MongoService,
    private redis: RedisHelperService,
    private address: AddressService,
  ) {}

  async updateTxsCount({ asset, txs }: TxsCountMsg): Promise<boolean> {
    this.log({ method: 'updateTxsCount', data: { asset, txs } });

    const provider = this.pg.getTrxProvider();
    const trx = await provider();
    const isExist = await this.checkTokenExistence(asset, provider);
    if (isExist) {
      const newCount = isExist.txs + txs;
      return this.pg
        .updateToken({ asset, update: { txs: newCount } }, provider)
        .then(res => {
          if (res) trx.commit();
          else trx.rollback();
          return res;
        })
        .catch(() => {
          trx.rollback();
          return false;
        });
    } else {
      this.logger.log(`${asset} is not in db, try creating one.`);
      await this.createTokenByAssetId(asset, provider);
      trx.rollback();
      return this.updateTxsCount({ asset, txs });
    }
  }

  async updateHoldersCount(stats: {
    asset: string;
    count: number;
  }): Promise<boolean> {
    const { asset, count } = stats;

    this.log({ method: 'updateHoldersCount', data: stats });

    const provider = this.pg.getTrxProvider();
    const trx = await provider();
    const isExist = await this.checkTokenExistence(asset, provider);

    if (isExist) {
      const holders = (+isExist.holders || 0) + count;
      return this.pg
        .updateToken({ asset, update: { holders } }, provider)
        .then(res => {
          if (res) trx.commit();
          else trx.rollback();
          return res;
        });
    }
    // create token then update token
    await this.createTokenByAssetId(asset, provider);
    return this.updateHoldersCount(stats);
  }

  // create new token issued on fusion
  async createToken(txHash: string): Promise<boolean> {
    this.log({ method: 'createToken', data: txHash });

    const provider = this.pg.getTrxProvider();

    const { hash, ...others } = await this.mongo
      .getTokenInfo(txHash)
      .catch(e => {
        this.logger.verbose(e);
        return { hash: txHash };
      });

    const isExist = await this.checkTokenExistence(hash, provider);
    if (isExist) return true;

    const snapshot = await this.rpc.getTokenInfo(hash);
    const tokenInfo: TokenInfo = {
      ...others,
      ...snapshot,
      hash,
    };
    return this.pg.createToken(tokenInfo, provider);
  }

  async createTokenByAssetId(assetId: string, provider: any) {
    this.log({ method: 'createTokenByAssetId', data: { asset: assetId } });
    const asset = await this.getTokenInfo(assetId);
    const trx = await provider();
    return this.pg
      .createToken(asset, provider)
      .then(res => (res ? trx.commit() : trx.rollback()))
      .catch(() => trx.rollback());
  }

  // track erc20
  async trackToken(token: string): Promise<boolean> {
    this.log({ method: 'trackToken', asset: token });

    const inRedis = await this.redis.getCachedValue(token);
    if (inRedis) return true;

    const provider = this.pg.getTrxProvider();
    const isExist = await this.checkTokenExistence(token, provider);
    if (isExist) {
      const trx = await provider();
      trx.commit();
      return true;
    }

    const tokenInfo: TokenInfo | null = await this.getTokenInfo(token);
    // No Token info, this address is not an erc20 token
    if (!tokenInfo) {
      // tell address worker that this address is not an erc20 address
      this.address.updateAddressInfo({ hash: token, erc20: false });
      // Ack msg direclty for false erc20 address
      return true;
    }
    return this.pg.createToken(tokenInfo, provider).then(res => {
      this.cacheErc20Existence(token);
      return res;
    });
  }

  // track fusion token quantity change cmd
  // future may add erc20 support
  async updateTokenSupply(asset: string): Promise<boolean> {
    this.log({ method: 'updateTokenSupply', data: asset });

    const provider = this.pg.getTrxProvider();
    const trx = await provider();
    const qty = await this.getTokenSupply(asset);
    const isExist = await this.pg.checkExistence(asset, provider);
    if (isExist) {
      return this.pg
        .updateToken({ asset, update: { qty } }, provider)
        .then(res => {
          if (res) trx.commit();
          else trx.rollback();
          return res;
        })
        .catch(e => false);
    } else {
      await this.sleep(13000);
      return this.updateTokenSupply(asset);
    }
  }

  private getTokenSupply(asset: string): Promise<number> {
    const isErc20 = asset.length === 42;
    if (isErc20) return this.web3.getTokenSupply(asset);
    else return this.rpc.getTokenInfo(asset).then(data => +data.qty);
  }

  private getTokenInfo(token: string): Promise<TokenInfo> {
    const isErc20 = token.length === 42;
    if (isErc20) return this.web3.getTokenInfo(token);
    else return this.mongo.getTokenInfoByID(token);
  }

  private sleep(ms: number): Promise<void> {
    return new Promise(resolve => {
      let t1 = setTimeout(() => {
        clearTimeout(t1);
        t1 = null;
        resolve();
      }, ms);
    });
  }

  private async checkTokenExistence(token: string, provider: any) {
    const isExist = await this.pg.checkExistence(token, provider);

    if (isExist) {
      if (token.length === 42) this.cacheErc20Existence(token);
      return isExist;
    }
  }

  private cacheErc20Existence(token: string) {
    // cache for an hour
    this.redis.cacheValue(token, token, 3600);
  }

  private log({ method, data }: any): void {
    this.logger.log(method);
    this.logger.verbose(data);
  }
}
